import React from 'react';
import { View, Text, StyleSheet, ScrollView, Image, Button, TouchableNativeFeedback } from 'react-native';
import { wallet, common, auditLog, dashboard } from '../../helpers/styles'
import { AntDesign, FontAwesome5,
         MaterialIcons, Entypo } from 'react-native-vector-icons';
import { formatDate, formatPrice,
         dateToYMD, formatGetDayMedium,
         monthYear, emptyObject,
         capitalize, filterYear,
         filterMonth } from '../../helpers/filters'
import moment from 'moment'         
import axios  from 'axios'         
import _  from 'lodash'         

const initialState = {
  wallet: {
    "balance": 0,
    "color": null,
    "created_by": null,
    "created_on": null,
    "currency": null,
    "id": null,
    "name": null,
    "order": 0,
    "starting_balance": 0,
    "status": null,
    "type": null,
    "user": {
      "display": null,
      "first_name": null,
      "is_mine": null,
      "last_name": null,
    },
    "user_id": null,
  },
  transactions:{},
  date_filter:null,
  dates:[],
  cardLoader:false,
};

export default class AuditLog extends React.Component {
  
  _isMounted = false;

  constructor(props){
    super(props);
    initialState.date_filter  = dateToYMD(new Date())
    initialState.wallet = this.props.activeWallet()
    if (!initialState.wallet.id) {
      this.props.changeView('home')
    }
    this.state = initialState;

  }

  selectWallet() {
    this.props.selectWallet(this.state.wallet)
  }

  componentDidMount() {
    
    this._isMounted = true;
    if (this._isMounted) {
      this.props.onRef(this);
      if (global.user) {
        global.reload_page = this.props.getParent()
        this.reloadPage()
      }
    }
  }

  async getWallet() {
    const wallet = this.state.wallet

    try {
      const response = await axios.post(global.app_url+'/api/a/wallet/data', wallet)
      let state = this.state
      state.wallet = response.data.wallet
      this.setState(state)
    } catch(e) {
      console.log(e)
    }

  }

  cardLoader(value) {
    let state = this.state
    state.cardLoader = value
    this.setState(state)
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
    this._isMounted = false;
  }

  transferWallet() {
    const wallet = this.state.wallet
    const transferWallet = {
      "available_funds": {
        "currency": wallet.currency,
        "value": wallet.balance,
      },
      "color": wallet.color,
      "label": wallet.name,
      "order": wallet.order,
      "owner_id": wallet.user_id,
      "starting_balance": {
        "amount": {
          "currency": wallet.currency,
          "value": wallet.starting_balance,
        },
      },
      "status": wallet.status,
      "type": wallet.type,
      "wallet_id": wallet.id,
    }

    this.props.transferWallet(transferWallet)
  }

  async getTransactions() {
    const user  = global.user;

    let data = {
      date_filter     : this.state.date_filter,
      organization_id : user.auth_organization.organization_id,
      wallet_id       : this.state.wallet.id,
    }
    this.cardLoader(true)
    await this.clearStates()

    try {
      const response = await axios.post(global.app_url+'/api/a/wallet/transactions', data)
      let state = this.state
      if (response.data.mapped_transaction) {
        state.transactions = response.data.mapped_transaction
        _.forOwn(state.transactions, function(transaction, date){
          state.dates.push(date)  
        })
        this.setState(state)
      }
      this.cardLoader(false)

    } catch(e) {
      console.log(e)
      this.cardLoader(false)
    }

  }

  clearStates() {
    let state = this.state
    state.transactions = {}
    state.dates = []
    this.setState(state)
  }

  async reloadPage() {
    global.reload_page = this.props.getParent()
    await this.getTransactions()
  }

  renderTransactionList(item) {
   const component = 
    <TouchableNativeFeedback
      key={item.transaction_id}
      background={TouchableNativeFeedback.Ripple('#dedede', false)}>
      <View style={{}} >
        <View style={{flexDirection: 'row',width:'100%',marginTop:10, }}>
          <View  style={dashboardStyle.mt_description}>
            <Text style={{fontSize:15,}}>{item.source.description}</Text>
            <View style={dashboardStyle.mt_moreDetail}>
              <View style={{width:'100%',flexDirection: 'row'}}>
                <Text style={{width:'50%',}}>
                  {item.source.category ? <Text style={dashboardStyle.mt_category} ><Entypo name="archive" size={12} /> {item.source.category}</Text> : <Text style={dashboardStyle.mt_category}><MaterialIcons name="merge-type" size={12} />  {capitalize(item.type)}</Text>}
                </Text>
                <Text style={{width:'50%',}}>
                  {item.source.merchant ? <Text style={dashboardStyle.mt_category} ><Entypo name="shop" size={12} /> {item.source.merchant}</Text> : null}
                </Text>
              </View>
            </View>  
          </View>
          <View style={dashboardStyle.mt_price}>
            <Text style={this.listColor(item.type, 'amount')} >
              {formatPrice(item.amount.value)}
            </Text>
            <Text style={this.listColor(item.type, 'currency')} >{item.amount.currency} </Text>
          </View>
        </View>
      </View>
    </TouchableNativeFeedback>
    return (component)
  }

  listColor(type, style) {
    if ('income' === type || 'receive' === type) {
      if ('currency' === style) {
        return dashboardStyle.mt_currency_label_income
      }else if ('amount' === style) {
        return dashboardStyle.mt_income
      }
    }else if ('expense' === type || 'transfer' === type) {
      if ('currency' === style) {
        return dashboardStyle.mt_currency_label_expense
      }else if ('amount' === style) {
        return dashboardStyle.mt_expense
      }
    }
  }


  // filter date start
  async monthProceed(action) {
    let date = null
    if (action == 'next') {
      date = moment(String(this.state.date_filter)).add(1, 'M').format('MMMM DD, Y')
    }else if (action == 'prev') {
      date = moment(String(this.state.date_filter)).add(-1, 'M').format('MMMM DD, Y')
    }
    let state = this.state
    state.date_filter = dateToYMD(new Date(date))
    await this.setState(state)
    this.getTransactions()
  }
  // filter date end

  transactionBody() {

    const _this = this
    const transactions = this.state.transactions
    let showTransactionNull = null
    if (transactions) {
      showTransactionNull = !emptyObject(transactions)
    }

    return (
      <View  style={dashboardStyle.transactions_container}>
      {showTransactionNull ? this.state.dates.map(function(date, key) {
        return <View key={key}>
            <View style={dashboardStyle.mt_row} >
              <Text><Text style={dashboardStyle.sd_day}>{formatDate(date)}</Text>  {(formatGetDayMedium(date))}</Text>
            </View>
            {transactions[date].map((item)=> (_this.renderTransactionList(item)) )}
          </View>
      }) : (<Text>No transactions for this month.</Text>)}
      </View>
    )
  }

  render() {
    return (
      <View style={{}}>
        <View style={walletStyle.screenHeader}>
          <View style={styles.textLabel}>
            <Text style={{fontWeight: 'bold',fontSize: 25 }}>{this.state.wallet.name}</Text>
            <Text style={{}}>{capitalize(this.state.wallet.type)}</Text>
          </View>
          <View style={{flexDirection: 'row-reverse',width:'48%',marginTop:'2%'}}>
            <Text style={{fontWeight: 'bold',fontSize: 25, }}>{formatPrice(this.state.wallet.balance)}</Text>
            <Text style={{fontWeight: 'bold',marginTop:'1.5%' }}>{this.state.wallet.currency} </Text>
          </View>
        </View>
        <View style={{paddingRight:'5%',paddingLeft:'3%',}}>
          <View style={{flexDirection: 'row',marginTop:10,width:'100%'}}>
          <View style={styles.btnEdit} >
            <Button title="EDIT" onPress={() => this.selectWallet()} color={'#53b3bd'} />
          </View>
          <View style={styles.btnTransfer} >
            <Button title="TRANSFER" style={styles.btnTransfer} onPress={() => this.transferWallet()} color={'#53b3bd'} />
          </View>
          
          </View>
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{padding:10,marginBottom: '35%'}}>
            <View style={dashboardStyle.transactionCard}>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row',width:'80%',marginLeft:5, marginTop:5,marginBottom:5,}}>
                    <FontAwesome5 onPress={() => {
                    this.monthProceed('prev')
                  }} name="angle-double-left" color='black' size={20} />
                    <Text style={{marginLeft:20,marginRight:20,}}>
                      {monthYear(this.state.date_filter)}
                    </Text>
                    <FontAwesome5 onPress={() => {
                    this.monthProceed('next')
                  }} name="angle-double-right" color='black' size={20} />
                </View>
              </View>
              {this.state.cardLoader ? (
                  <Image style={{ width: 100, height: 50,marginLeft:'38%', resizeMode: 'contain' }} source={require('../../../../assets/Spinner-0.3s-58px.gif')} />
                ) : this.transactionBody()}
            </View>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const commonStyle    = StyleSheet.create(common);
const walletStyle    = StyleSheet.create(wallet);
const styles         = StyleSheet.create(auditLog)
const dashboardStyle = StyleSheet.create(dashboard)
