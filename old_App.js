import React from 'react';
import { StyleSheet, Text, View, TextInput, Button } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style={styles.formInputs}>
          <TextInput placeholder="Course Goal"  style={styles.textInput}/>
          <Button title="ADD" />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding:30,
  },
  formInputs:{
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center', 
  },
  textInput:{
    borderColor: 'black',
    borderWidth: 1,
    padding:5,
    width:'80%',
  },
});
