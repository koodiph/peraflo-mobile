
import React from 'react';
import { KeyboardAvoidingView, Platform, ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid, Picker, Alert  } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";
import DatePicker               from 'react-native-datepicker'
import DateTimePicker           from '@react-native-community/datetimepicker';

// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign     from 'react-native-vector-icons/AntDesign';
import moment        from 'moment'
import Select2       from 'react-native-select-two';
import Toast         from 'react-native-simple-toast';

const initialState = {
  auth_organization:{},
  transaction:{
      'transaction_date':'',
      'currency':'',
      'description':'',
      'attachment':'',
      'type':'expense',
      'user_id':'',
      'organization_id':'',
      'wallet_id':'',
      'reimbursable':'',
      'category_id':'',
      'amount':'',
      'tags':'',
      'file_attachment':'',
      'merchants':'',
  },
  showDate:false,
  datePickerCount:{
    transaction_date:0,
  },
  wallets:[],
};

const mockData = [
    { id: 1, name: 'React Native Developer' },
    { id: 2, name: 'Android Developer' },
    { id: 3, name: 'iOS Developer' }
];

export default class LedgerEntry extends React.Component {


  constructor(props){
    super(props);
    global.view = 'ledger_entry'
    if (!global.user) {
        // this.props.logout()
    }
    var transaction_data           = this.props.getTransaction()
    var wallets                    = this.props.getWallets()
    initialState.auth_organization = global.user.auth_organization
    initialState.wallets           = wallets
    if (transaction_data && transaction_data.id) {
      initialState.transaction = transaction_data
    }else{
      initialState.transaction.transaction_date = this.dateToYMD(new Date())
      initialState.transaction.type             = 'expense'
      initialState.transaction.user_id          = global.user.id
      initialState.transaction.organization_id  = global.user.auth_organization.id
      initialState.transaction.currency         = global.user.auth_organization.settings.currency[0].code
      initialState.transaction.category_id      = global.user.auth_organization.settings.categories.expense[0].id
    }

    this.state         = initialState;
    global.reload_page = this
  }



  componentDidMount() {
    var _this = this
    const initialState = {
      auth_organization:{},
      transaction:{
          'transaction_date':'',
          'currency':'',
          'description':'',
          'attachment':'',
          'type':'expense',
          'user_id':'',
          'organization_id':'',
          'wallet_id':'',
          'reimbursable':'',
          'category_id':'',
          'amount':'',
          'tags':'',
          'file_attachment':'',
          'merchants':'',
      },
      showDate:false,
      datePickerCount:{
        transaction_date:0,
      },
    }

    var transaction_data           = this.props.getTransaction()
    var wallets                    = this.props.getWallets()
    initialState.auth_organization = global.user.auth_organization
    initialState.wallets           = wallets
    if (transaction_data.id) {
      initialState.transaction = transaction_data
      initialState.transaction.wallet_id = 'parent_'+transaction_data.wallet_id
      if (transaction_data.merchant) {
        initialState.transaction.merchants = transaction_data.merchant.name
      }
    }else{
      initialState.transaction.transaction_date = this.dateToYMD(new Date())
      initialState.transaction.type             = 'expense'
      initialState.transaction.user_id          = global.user.id
      initialState.transaction.organization_id  = global.user.auth_organization.id
      initialState.transaction.currency         = global.user.auth_organization.settings.currency[0].code
      initialState.transaction.category_id      = global.user.auth_organization.settings.categories.expense[0].id
    }

    _this.setState(initialState)
  }

  dateToYMD (date) {
      var d = date.getDate();
      var m = date.getMonth() + 1; //Month from 0 to 11
      var y = date.getFullYear();
      return '' + y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
  }

  reloadPage(){
    this.changeView('dashboard')
  }

  changeView(view){
    var _this = this
    _this.props.selectTransaction({id:null});

    var transaction = {
          'id':'',
          'transaction_date':'',
          'currency':'',
          'description':'',
          'attachment':'',
          'type':'expense',
          'user_id':'',
          'organization_id':'',
          'wallet_id':'',
          'reimbursable':'',
          'category_id':'',
          'amount':'',
          'tags':'',
          'file_attachment':'',
          'merchants':'',
      }

    var state = _this.state
    state.transaction = transaction
    _this.setState(state)
    _this.props.changeView(view);
  }

  confirmDelete(){
    Alert.alert(
      "Are you sure?",
      "You won't be able to revert this!",
      [
        { text: "Yes, delete it!", onPress: () => this.deleteTransaction() },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
      ],
      { cancelable: false }
    );
  }

  deleteTransaction(){
    var _this = this    
    var state = _this.state
    var data = state.transaction
    var url = '/api/a/transaction/delete'
    axios.post(global.app_url+url, data).then(function(response){
      _this.changeView('dashboard')
      Toast.show('Entry sucessfully deleted!');
    }).catch(function(error){
      console.log(error.response.data)
      Toast.show(error.response.data);
    });   
  }

  setSelecteOption(selected, field){
    var _this = this
    var state = _this.state
    state.transaction[field] = selected
    if (field =='type' ) {
      state.transaction.category_id = global.user.auth_organization.settings.categories.income[0].id
    }
    _this.setState(state)
  }

  setSelectedDate(date){
    var _this = this
    if (date) {
      _this.setSelecteOption(_this.dateToYMD(new Date(date)),'transaction_date')
    }
    var state      = _this.state
    state.showDate = false
    _this.setState(state)
  }

  receiptUpload(file){

  }

  changeType(type){
    this.setSelecteOption(type,'type')
  }

  saveTransaction(){
    var _this = this
    var validate_field = true
    var required_fields = [
        'currency',
        'description',
        'amount',
        'transaction_date',
    ];

    var state = _this.state
    if (
      !state.transaction.currency ||
      !state.transaction.description ||
      !state.transaction.amount ||
      // !state.transaction.category_id ||
      !state.transaction.transaction_date
    ) {
      validate_field = false
    }

    if (!validate_field) {
      Toast.show('Fill up required fields');
      return;
    }
    
    if (validate_field) {
        var data = state.transaction
        var url = '/api/a/transaction/store'
        if (data.id) {
          url = '/api/a/transaction/update'
        }
        axios.post(global.app_url+url, data).then(function(response){
          _this.changeView('dashboard')
        }).catch(function(error){
          Toast.show(error.response.data);
        });        
    }
  }

  showDate()
  {
    var _this      = this
    var state      = _this.state
    state.showDate = true
    _this.setState(state)
  }

  formatPrice(value) {
    if (value) {
        let val = (value/1).toFixed(2).replace(',', '.')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }else{
        return '0.00'
    }
  }
  
  render() {
    var type_selector = <View style={styles.type_select}>
          <View style={styles.ts_selected}  onPress={() => this.changeType('expense')}>
            <Text style={{color:'#33929c'}} onPress={() => this.changeType('expense')}>Expense</Text>
          </View>
          <View style={styles.ts_select} onPress={() => this.changeType('income')}>
            <Text style={{color:'#33929c'}} onPress={() => this.changeType('income')}>Income</Text>
          </View>
        </View>
    let categoriesItem = this.state.auth_organization.settings.categories.expense.map( (category, key) => {
        return <Picker.Item key={key} value={category.id} label={category.name} />
    });
    if (this.state.transaction.type == 'income') {
          type_selector = <View style={styles.type_select}>
          <View style={styles.ts_select}  onPress={() => this.changeType('expense')}>
            <Text style={{color:'#33929c'}} onPress={() => this.changeType('expense')}>Expense</Text>
          </View>
          <View style={styles.ts_selected} onPress={() => this.changeType('income')}>
            <Text style={{color:'#33929c'}} onPress={() => this.changeType('income')}>Income</Text>
          </View>
        </View>
        categoriesItem = this.state.auth_organization.settings.categories.income.map( (category, key) => {
            return <Picker.Item key={key} value={category.id} label={category.name} />
        });
    }


    let currencyItems = this.state.auth_organization.settings.currency.map( (currency, key) => {
        return <Picker.Item key={key} value={currency.code} label={currency.code} />
    });

    let walletsItem = this.state.wallets.map( (wallet, key) => {
        return <Picker.Item key={key} value={'parent_'+wallet.id} label={wallet.name+' - '+wallet.currency+' '+this.formatPrice(wallet.current_amount)} />
    });
    
    return (
      <KeyboardAvoidingView 
      behavior={"height"}
      style={styles.avoid_view}
      >
      <View style={styles.body}>
        <View style={styles.top_back_btn}>
          <View style={styles.back_btn}>
              <AntDesign style={styles.tbb_icon} name="back" color='black' size={15}  onPress={() => this.changeView('dashboard')} />
              <Text style={styles.tbb_back} onPress={() => this.changeView('dashboard')} >Back</Text>
          </View>
          {this.state.transaction.id ? (<View style={styles.delete_btn}>
              <Text style={styles.tbb_delete} onPress={() => this.confirmDelete()} >Delete</Text>
              <AntDesign style={styles.tbb_icon} name="delete" color='black' size={15}  onPress={() => this.confirmDelete()} />
          </View>) : null}
          
        </View>
        {type_selector}
        <View style={styles.form_input}>
        <ScrollView>
          <Text style={styles.fi_label}>Amount <Text style={{color:'red'}}>*</Text></Text>
          <View style={styles.fi_row}>
            <View
              style={styles.fi_picker}
              >
              <Picker
                selectedValue={this.state.transaction.currency}
                onValueChange={(itemValue, itemIndex) => this.setSelecteOption(itemValue,'currency')}
              >
              {currencyItems}
              </Picker>
            </View>
              <TextInput  
                style={styles.fi_textinput}  
                editable={this.state.transaction.id ? false: true}
                keyboardType={'numeric'}  
                defaultValue={''+this.state.transaction.amount}
                onChangeText={(inputText) => this.setSelecteOption(inputText,'amount')}
              />  
          </View>
          <Text style={styles.fi_label}>Description <Text style={{color:'red'}}>*</Text></Text>
          <View style={styles.fi_row}>
              <TextInput  
                defaultValue={this.state.transaction.description}
                onChangeText={(inputText) => this.setSelecteOption(inputText,'description')}
                style={styles.fi_input_full_width}  
              />  
          </View>
          <View style={styles.fi_row}>
            <Text style={styles.fi_r_date_label}>Date <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_r_date_input}>
              <Text  onPress={() => this.showDate()}>{this.state.transaction.transaction_date} </Text>
            </View>
            {this.state.showDate ? (<DateTimePicker
              testID="dateTimePicker"
              mode={'calendar'}
              value={new Date(this.state.transaction.transaction_date)}
              display="default"
              onChange={(event, date) => {
                this.setSelectedDate(date)
              }}
            />) : null }
          </View>
          <View style={styles.fi_row}>
            <Text style={styles.fi_r_label}>Wallet</Text>
              <View
              style={styles.fi_half_picker}
              >
              <Picker
                selectedValue={this.state.transaction.wallet_id}
                onValueChange={(itemValue, itemIndex) => this.setSelecteOption(itemValue,'wallet_id')}
              >
              <Picker.Item value={null} label={'Select wallet'} enabled={false}/>
              {walletsItem}
              </Picker>
              </View>
          </View>
          <View style={styles.fi_row}>
            <Text style={styles.fi_r_label}>Category </Text>
              <View
              style={styles.fi_half_picker}
              >
              <Picker
                selectedValue={this.state.transaction.category_id}
                onValueChange={(itemValue, itemIndex) => this.setSelecteOption(itemValue,'category_id')}
              >
                {categoriesItem}
              </Picker>
              </View>
          </View>
          <View style={styles.fi_row}>
            <Text style={styles.fi_r_label}>Merchant</Text>
              <View
              style={styles.fi_half_picker}
              >
              <TextInput  
                defaultValue={this.state.transaction.merchants}
                onChangeText={(inputText) => this.setSelecteOption(inputText,'merchants')}
                style={styles.fi_input_full_width}  
              />  
              </View>
          </View>
          <View style={styles.fi_row}>
          </View>
        </ScrollView>
        <View style={styles.button_position}>
          <Button title="save" color="#53b3bd" onPress={() => {
            this.saveTransaction()
          }}/>
        </View>
        </View>
      </View>
      </KeyboardAvoidingView>
    );
  }

}



const styles = StyleSheet.create({
  avoid_view:{
    flex:1,
  },
  body: {
    padding:10,
  },
  top_back_btn:{
    flexDirection: 'row', 
  },
  back_btn:{
    flexDirection: 'row', 
    width:'50%',
  },
  delete_btn:{
    flexDirection: 'row', 
    width:'50%',
  },
  tbb_back:{
    marginLeft:5,
  },
  tbb_delete:{
    marginLeft:'65%',
  },
  tbb_icon:{
    marginLeft:5,
    marginTop:1,
  },
  button_position:{
    marginTop:10,
  },
  type_select:{
    flexDirection: 'row', 
    marginTop: 10,
  },
  ts_selected:{
    width:'50%',
    textAlign: 'center',
    alignItems: 'center',
    fontSize: 25,  
    backgroundColor: '#c4eaef',
    color:'#33929c',
    paddingTop:10,
    height: '100%',
  },
  ts_select:{
    width:'50%',
    textAlign: 'center',
    alignItems: 'center',
    fontSize: 25,  
    color:'#33929c',
    paddingTop:10,
    paddingBottom: 10,
  },
  form_input:{
    height: '85%',
    borderWidth: 1,
    borderColor:'#c4eaef',
    padding:10,
    marginBottom:50,
  },
  fi_row:{
    flexDirection: 'row', 
    marginTop: 10,
    marginBottom: 10,
  },
  fi_picker:{
    height: 50,
    marginRight: 20,
    width: '30%',
    borderWidth: 1,
    borderColor: '#dedede',
  },
  fi_textinput:{
    borderWidth: 1,
    borderColor: '#dedede',
    width: '65%',
    height: 50,
    padding:5,
    marginTop:0,
  },
  fi_label:{
  },
  fi_r_date_input:{
    borderWidth:0.5,
    borderColor:'black',
    width:'62%',
    padding:10,
    marginLeft:40,
  },
  fi_input_full_width:{
    borderWidth: 1,
    borderColor: '#dedede',
    width: '100%',
    height: 50,
    padding:5,
  },
  fi_input_half_width:{
    borderWidth: 1,
    borderColor: '#dedede',
    width: '75%',
    height: 35,
    padding:5,
  },
  fi_r_date_label:{
    width: '25%',
    marginTop: 6,
  },
  fi_date_picker:{
    width: '75%',
  },
  fi_half_picker:{
    height: 50,
    width: '65%',
    borderWidth: 1,
    borderColor: '#dedede',
  },
  fi_r_label:{
    width: '35%',
    marginTop: 13,
  },
});
