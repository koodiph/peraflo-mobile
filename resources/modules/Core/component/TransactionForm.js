import React, { useState } from 'react';
import { KeyboardAvoidingView, Platform,
         View, Button,
         Text, Picker,
         StyleSheet, TextInput,
         TouchableOpacity, SafeAreaView,
         Keyboard, Alert,
         ScrollView } from 'react-native';
import { FontAwesome5, Entypo,
         FontAwesome, Fontisto,
         MaterialIcons, MaterialCommunityIcons,
         AntDesign } from 'react-native-vector-icons';
import { Modal, ModalContent,
         BottomModal, ModalFooter,
         ModalButton }  from 'react-native-modals';
import { capitalize, formatPrice,
         dateToYMD, previewText,
         specialFormat, cleanDecimal } from '../../helpers/filters'
import { common, transactionForm } from '../../helpers/styles'

import Autocomplete from 'react-native-autocomplete-input';
import axios  from 'axios';
import DateTimePicker from '@react-native-community/datetimepicker';
import Toast from 'react-native-simple-toast';
import DropDownPicker from 'react-native-dropdown-picker';

import _ from 'lodash';

const initialState = {
  form:{
    amount:'0.00',
    currency:'',
    postDate:'',
    description:'',
    wallet:'',
    category:'',
    receipt:'',
    tags:'',
    merchant:'',
  },
  modalVisible:false,
  formSelection:'expense',
  currencies:[],
  view:'form',
  showDate:false,
  wallets:[],
  categories:[],
  merchants:[],
  tags:[],
  amountFocus:false,
  descriptionFocus:false,
  validForm:false,
  loader:false,
  transactionType:'',
  loans:{},
  loanReceives:{},
  bills:[],
  sameFields: [
    'income',
    'expense',
  ],
  transfer:{
    fromWallet:{},
    toWallet:{},
  },
  bill:{},
  loan:{},
  selectedTransaction:{},
  pickerSelection:null,
  moreForm:false,
  screenTransfer:{},
};

export default class TransactionForm extends React.Component {

  _isMounted = false;

  componentDidMount() {
    
    this._isMounted = true;

    if (this._isMounted) {
      this.props.onRef(this);
      const _this = this

      this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', function(){
        _this.blurFormatPrice()
        _this.descriptionBlur()
      });
    }
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
    this.keyboardDidHideListener.remove();
    this._isMounted = false;

  }

  constructor(props){
    super(props);
    initialState.form.postDate  = dateToYMD(new Date())
    if (global.user) {
      initialState.currencies   = global.user.auth_organization.currencies
      initialState.form.currency = initialState.currencies[0].code 
    }

    this.state = initialState;
  }

  selectedTransaction(transaction) {
    let state = this.state
    state.selectedTransaction = transaction
    this.setState(state)
    this.mapTransactionSelected()
  }

  async mapTransactionSelected() {
    let state = this.state

    const transaction = this.state.selectedTransaction
    let transactionType = this.state.transactionType
    let formSelection = this.state.formSelection
    let transfer = this.state.transfer

    const sameFields = this.state.sameFields
    let form = {
      amount:formatPrice(transaction.amount.value),
      currency:transaction.amount.currency,
      postDate:transaction.post_date,
      description:transaction.source.description,
      wallet:'',
      category:'',
      receipt:'',
      tags:'',
      merchant:transaction.source.merchant,
    }

    if (transaction.source.tags.length) {
      form.tags = transaction.source.tags[0]
    }

    if (transaction.wallet.wallet_id) {
      form.wallet = transaction.wallet.wallet_id
    }

    if (transaction.source.category.id) {
      form.category = transaction.source.category.id
    }

    state.form = form

    transactionType = transaction.source.reference.type
    formSelection   = transaction.type

    if (transactionType) {
      formSelection = 'more'
      if ('loan' === transactionType && 'income' === formSelection) {
        transactionType = 'loan-receive'
      }else if ('loan' === transactionType && 'expense' === formSelection) {
        transactionType = 'loan'
      }
      state.pickerSelection = transaction
    }

    if (!transactionType) {
      transactionType = ''
    }

    if ('transfer' === transactionType) {
      transfer.fromWallet = transaction.from_wallet
      transfer.toWallet = transaction.to_wallet
      if (!transfer.fromWallet) {
        transfer.fromWallet = transaction.wallet
      }
      if (!transfer.toWallet) {
        transfer.toWallet = transaction.wallet
      }

      state.transfer = transfer
    }
    state.formSelection = formSelection
    state.transactionType = transactionType

    this.setState(state)
    
    await this.setModalVisible(false)
  }

  async setModalVisible(clearForm = true) {
    if (!this.state.modalVisible) {
      await this.getTransactionDetails()
    }

    let state = this.state
    state.modalVisible = !this.state.modalVisible
    this.setState(state)
    await this.validateForm()
    if (clearForm) {
      this.resetForm()
    }
  }

  async getTransactionDetails() {
    const url = window.app_url + '/api/a/transaction-details'
    const user = global.user
    const auth_organization = user.auth_organization

    let data = {
      organization_id : user.auth_organization.organization_id,
      role            : user.role.name
    }


    if (undefined == data.organization_id) {
      data.organization_id = user.auth_organization.id
    }


    try {
      const response = await axios.post(url, data)
      let state          = this.state
      state.wallets      = response.data.wallets
      state.categories   = response.data.categories
      state.merchants    = response.data.merchants
      state.tags         = response.data.tags
      state.currencies   = response.data.currencies
      state.loans        = response.data.loans
      state.loanReceives = response.data.loan_receives
      state.bills        = response.data.bills

      if (state.currencies.length) {
        state.form.currency = state.currencies[0].code
      }
      this.setState(state)
    } catch(e) {
      console.log(e)
    }

  }

  resetForm() {
    let state = this.state

    let form = {
      amount:'0.00',
      currency:initialState.currencies[0].code,
      postDate:dateToYMD(new Date()),
      description:'',
      wallet:'',
      category:'',
      receipt:'',
      tags:'',
      merchant:'',
    }

    state.formSelection = 'expense'
    state.transactionType = ''
    state.form = form;
    state.transfer = {
      fromWallet:{},
      toWallet:{},
    }
    state.selectedTransaction = {}
    state.pickerSelection = null
    state.moreForm = false
    state.screenTransfer = {}

    this.setState(state)
  }

  changeFormSelection(type) {
    let state = this.state
    state.formSelection = type
    let form = {
      amount:'0.00',
      currency:initialState.currencies[0].code,
      postDate:dateToYMD(new Date()),
      description:'',
      wallet:'',
      category:'',
      receipt:'',
      tags:'',
      merchant:'',
    }
    state.form = form;
    state.transactionType = ''
    state.transfer = {
      fromWallet:{},
      toWallet:{},
    }
    state.selectedTransaction = {}
    state.loan = {}
    state.bill = {}
    state.pickerSelection = null

    this.setState(state)

    this.validateForm()
  }

  headerStyle() {
    const type = this.state.formSelection
    if ('expense' === type) {
      return styles.formHeaderExpenseRow
    }else if ('income' === type) {
      return styles.formHeaderIncomeRow
    } else {
      return styles.formHeaderMoreRow
    }
  }

  inputForm(type) {
    let state = this.state
    const form = this.state.form
    if (form[type]) {
      state.input.value = form[type]
    }
    state.input.type = type
    state.view = 'field'
    this.setState(state)
  }

  blurFormatPrice() {
    let form = this.state.form

    form.amount = formatPrice(form.amount.replace(/[^.\d]/g, ''))

    let state = this.state
    state.form = form
    state.amountFocus = false
    this.setState(state)

    this.validateForm()
  }

  inputValue(value, field){
    let form = this.state.form
    form[field] = value
    let state = this.state
    if ('postDate' === field) {
      if (value) {
        form[field] = dateToYMD(value)
      }else{
        form[field] = dateToYMD(new Date())
      }
      this.selectDate()
    }

    if ('category' === field && 'parent-category' === value) {
      form[field] = ''
    }

    state.form = form
    this.setState(state)

    this.validateForm()
  }
  
  validateForm() {
    const form = this.state.form
        
    const requiredFields = [
      'amount',
      'currency',
      'postDate',
      'description',
    ]

    let validation = true

    _.forOwn(requiredFields,function(field){
      if (validation && !form[field]) {
        validation = false
      }

      if (validation && 'amount' === field && '0.00' == form[field]) {
        validation = false
      }
    })

    if ('more' === this.state.formSelection && 'transfer' === this.state.transactionType) {
      const transfer   = this.state.transfer
      const fromWallet = transfer.fromWallet
      const toWallet   = transfer.toWallet
      if (validation && !fromWallet.wallet_id) {
        validation = false
      }

      if (validation && !toWallet.wallet_id) {
        validation = false
      }
    }

    let state = this.state
    state.validForm = validation
    this.setState(state)
  }

  selectDate() {
    let state = this.state
    state.showDate = !state.showDate
    this.setState(state)
  }

  clearAmount() {
    const amount = this.state.form.amount
    let form = this.state.form
    let state = this.state

    if ('0.00' === amount) {
      form.amount = ''
    }
    state.amountFocus = true
    state.form = form
    this.setState(state)
  }
  
  descriptionBlur() {
    let state = this.state
    state.descriptionFocus = false
    this.setState(state)
    this.validateForm()
  }

  descriptionFocus() {
    let state = this.state
    state.descriptionFocus = true
    this.setState(state)
  }

  loader(value) {
    let state = this.state
    state.loader = value
    this.setState(state)
  }

  transactionTypeChange(value) {
    let state = this.state
    state.transactionType = value
    state.bill = {}

    let form = {
      amount:'0.00',
      currency:initialState.currencies[0].code,
      postDate:dateToYMD(new Date()),
      description:'',
      wallet:'',
      category:'',
      receipt:'',
      tags:'',
      merchant:'',
    }

    state.form = form;

    this.setState(state)
  }

  setBill(value) {
    let state = this.state
    let form  = this.state.form

    form.amount      = formatPrice(value.amount)
    form.currency    = value.currency
    form.description = value.description
    form.category    = value.category

    state.bill = value
    
    if ('bill-parent'=== value) {
      state.bill = {}
      let form = {
        amount:'0.00',
        currency:initialState.currencies[0].code,
        postDate:dateToYMD(new Date()),
        description:'',
        wallet:'',
        category:'',
        receipt:'',
        tags:'',
        merchant:'',
      }
    }

    state.form = form
    this.setState(state)

    this.validateForm()
  }

  async save() {
    const state = this.state
    const form = state.form
    const user = global.user
    const auth_organization = user.auth_organization

    if (!state.validForm) {
      Toast.show('Please fill up required fields.');
      return
    }
    let url = global.app_url+`/api/a/transaction/store`
    const loader = state.loader
    const formSelection = state.formSelection

    form.organization_id = user.auth_organization.organization_id

    const fieldSimilar = this.state.sameFields
    form.type = state.transactionType

    if (_.includes(fieldSimilar, formSelection)) {
      form.type = formSelection
    }

    if ('bill' === form.type) {
      form.billId = state.bill.bill_id
      url = global.app_url+`/api/a/transaction/paybill`
    }

    if ('loan' === form.type || 'loan-receive' === form.type) {
      form.loanId = state.loan.id
      url = global.app_url+`/api/a/transaction/payLoan` 
    }

    if ('more' === this.state.formSelection && 'transfer' === this.state.transactionType) {
      const transfer = this.state.transfer
      form.fromWalletId = transfer.fromWallet.wallet_id 
      form.toWalletId = transfer.toWallet.wallet_id 
      url = global.app_url+`/api/a/transaction/transfer` 
    }

    const transaction = this.state.selectedTransaction
    if (transaction.transaction_id) {
      form.transactionId = transaction.transaction_id
      url = global.app_url+`/api/a/transaction/update`
    }

    if (!loader) {
      this.loader(true)
      try {
        const response = await axios.post(url, form)
        if (!response.data.error) {
          
          if (this.state.screenTransfer && this.state.screenTransfer.wallet_id) {
            this.props.reloadWallet()
          }

          this.resetForm()
          this.setModalVisible();
          Toast.show(response.data.message);
          this.props.loadTransactions() 
          

        } else {
          if (response.data.message) {
            Toast.show(response.data.message);
          }else{
            Toast.show('Failed saving transaction.');
          }
        }
        this.loader(false)
      } catch(e) {
        console.log(e)
        Toast.show('Failed saving transaction.');
        this.loader(false)
      }
    }else{
      Toast.show('Currently processing another query.');
    }
  }

  async delete() {
    const _this = this
    const state = this.state
    const loader = state.loader
    const url = global.app_url+`/api/a/transaction/delete` 

    const data = {
      entryId:state.selectedTransaction.transaction_id,
      reference:state.selectedTransaction.source.reference,
    }
    Alert.alert(
      "Delete",
      "Are you sure?",
      [
        {
          text: "Yes",
          async onPress() {
            if (!loader) {
              _this.loader(true)
              try {
                const response = await axios.post(url, data)
                if (!response.data.error) {
                  _this.resetForm()
                  _this.setModalVisible();
                  _this.props.loadTransactions() 
                  Toast.show(response.data.message);
                }else{
                  if (response.data.error) {
                    Toast.show(response.data.message);
                  }else{
                    Toast.show('Failed deleting transaction.');
                  }
                }
                _this.loader(false)
              } catch(e) {
                Toast.show('Failed deleting transaction.');
                _this.loader(false)
              }
            }else{
              Toast.show('Currently processing another query.');
            }
          },
          style: "cancel"
        },
        { text: "Cancel", onPress() {
        }}
      ],
      { cancelable: false }
    );
  }
  
  pickerCurrencyComponent() {
    return (
      <View
          style={{
            paddingVertical: 8,
            backgroundColor: '#fff',
            borderRadius: 20,
            overflow: 'hidden', 
            textAlign: 'center', 
        }}>
        <Picker
          style={{height: 24,backgroundColor: '#fff',marginLeft:'25%', }}
          selectedValue={this.state.form.currency}
          onValueChange={(itemValue, itemIndex) => this.inputValue(itemValue, 'currency')}
        >
        {this.state.currencies.map((currency, key) => {
          return <Picker.Item value={currency.code} key={key} label={currency.code}/>
        })}
        </Picker>
      </View>
    )
  }

  datePickerComponent() {
    return (
      <View>
       {this.state.showDate ? (
        <DateTimePicker
          testID="dateTimePicker"
          mode={'calendar'}
          value={new Date(this.state.form.postDate)}
          display="default"
          onChange={(event, date) => {
            this.inputValue(date, 'postDate')
          }}
        />) : null }
       <Text onPress={()=> this.selectDate()}>{specialFormat(this.state.form.postDate)}</Text>
      </View>
    )
  }

  pickerWalletComponent() {
    return (
      <Picker
        style={styles.pickerInputCard}
        selectedValue={this.state.form.wallet}
        onValueChange={(itemValue, itemIndex) => this.inputValue(itemValue, 'wallet')}
      >
      <Picker.Item value={''}  key={1821} label={``}/>
      {this.state.wallets.map((wallet, key) => {
        return <Picker.Item value={wallet.wallet_id} key={key} label={`${wallet.label} - ${wallet.available_funds.currency} ${formatPrice(wallet.available_funds.value)}`}/>
      })}
      </Picker>
    )
  }

  pickerCategoryComponent() {
    return (
      <Picker
        style={styles.pickerInputCard}
        selectedValue={this.state.form.category}
        onValueChange={(itemValue, itemIndex) => this.inputValue(itemValue, 'category')}
      >
      {this.pickerCategoryItem()}
      </Picker>
    )
  }

  pickerCategoryItem() {
    const formSelection = this.state.formSelection
    let items = [];
    items.push(
      <Picker.Item value={''} key={1821} label={``}/>
    )

    const categories = this.state.categories;
    const transactionType = this.state.transactionType;

    const expenseCategories = [
      'loan',
      'bill',
    ]

    const incomeCategories = [
      'loan-receive',
    ]

    if ('expense' === formSelection || ('more' === formSelection && _.includes(expenseCategories, transactionType))) {
      _.forOwn(categories.expense,function(mainCategories, type){
        items.push(
          <Picker.Item value={'parent-category'} key={type} label={`${type}`}/>
        )
        mainCategories.map((category, key) => {
          items.push(
            <Picker.Item value={category.id} key={key} label={` - ${category.name}`}/>
          )
        })
      })      
    }else if ('income' === formSelection || ('more' === formSelection && _.includes(incomeCategories, transactionType))) {
      _.forOwn(categories.income,function(mainCategories, type){
        items.push(
          <Picker.Item value={'parent-category'} key={type} label={`${type}`}/>
        )
        mainCategories.map((category, key) => {
          items.push(
            <Picker.Item value={category.id} key={key} label={` - ${category.name}`}/>
          )
        })
      })
    }
    return (items)
  }

  tagInputComponent() {
    return (
      <Autocomplete
        data={this.state.tags}
        onChangeText={text => this.inputValue(text, 'tags')}
      />
    )
  }

  pickerTransactionTypeComponent() {
    return (
      <Picker
        style={styles.pickerInputCard}
        selectedValue={this.state.transactionType}
        onValueChange={(itemValue, itemIndex) => this.transactionTypeChange(itemValue)}
      >
        <Picker.Item value={'bill'} key={0} label={`Pay a bill`}/>
        <Picker.Item value={'loan'} key={1} label={`Pay a debt I owe`}/>
        <Picker.Item value={'loan-receive'} key={2} label={`Receive debt payment`}/>
        <Picker.Item value={'transfer'} key={3} label={`Transfer wallet's money`}/>
      </Picker>
    )
  }


  billPickerComponent() {
    let items = []
    items.push(
      <Picker.Item value={'bill-parent'} key={3} label={``}/>
    )
    const bills = this.state.bills
    
    bills.map(function(bill) {
        items.push(
          <Picker.Item value={bill} key={3} label={`${bill.description} - ${bill.currency}  ${formatPrice(bill.amount)}`}/>
        )
    })

    return (
      <Picker
        style={styles.pickerInputCard}
        selectedValue={this.state.bill}
        onValueChange={(itemValue, itemIndex) => this.setBill(itemValue)}
      >
      {items}
      </Picker>
    )
  }

  loanPickerComponent() {
    let items = []
    items.push(
      <Picker.Item value={'loan-parent'} key={3} label={``}/>
    )
    const loans = this.state.loans
    _.forOwn(loans, function(dataLoans, type){
      items.push(
        <Picker.Item value={'loan-parent'} key={3} label={capitalize(type)}/>
      )
      dataLoans.map(function(loan) {
        if (loan.installment_amount) {
          items.push(
            <Picker.Item value={loan} key={3} label={`- [${loan.installment_occurrence}/${loan.installment_total_frequency}] ${loan.description} - ${loan.value.currency}  ${formatPrice(loan.installment_amount)}`}/>
          )
        }else{
          items.push(
            <Picker.Item value={loan} key={3} label={`- ${loan.description} - ${loan.value.currency}  ${formatPrice(loan.value.amount)}`}/>
          )
        }
      })
    })
    return (
      <Picker
        style={styles.pickerInputCard}
        selectedValue={this.state.loan}
        onValueChange={(itemValue, itemIndex) => this.setLoan(itemValue)}
      >
      {items}
      </Picker>
    )
  }

  async transferWalletFromWalletScreen(wallet) {

    await this.setModalVisible()
    await this.changeFormSelection('more')
    await this.transactionTypeChange('transfer')
    const wallets = this.state.wallets

    let selectedWallet = null
    
    _.forOwn(wallets,function(wallt){
      if (wallt.wallet_id === wallet.wallet_id) {
        selectedWallet = wallt
      }
    })

    let state = this.state
    state.screenTransfer = wallet
    if (selectedWallet) {
      state.transfer.fromWallet = selectedWallet
    }
    this.setState(state)
  }

  loanReceivePickerComponent() {
    let items = []
    items.push(
      <Picker.Item value={'loan-parent'} key={3} label={``}/>
    )
    const loans = this.state.loanReceives
    _.forOwn(loans, function(dataLoans, type){
      items.push(
        <Picker.Item value={'loan-parent'} key={3} label={capitalize(type)}/>
      )
      dataLoans.map(function(loan) {
        if (loan.installment_amount) {
          items.push(
            <Picker.Item value={loan} key={3} label={`- [${loan.installment_occurrence}/${loan.installment_total_frequency}] ${loan.description} - ${loan.value.currency}  ${formatPrice(loan.installment_amount)}`}/>
          )
        }else{
          items.push(
            <Picker.Item value={loan} key={3} label={`- ${loan.description} - ${loan.value.currency}  ${formatPrice(loan.value.amount)}`}/>
          )
        }
      })
    })
    return (
      <Picker
        style={styles.pickerInputCard}
        selectedValue={this.state.loan}
        onValueChange={(itemValue, itemIndex) => this.setLoan(itemValue)}
      >
      {items}
      </Picker>
    )
  }

  setLoan(value) {
    let state = this.state
    let form  = this.state.form

    if ('loan-parent' === value) {
      state.loan = {}
      let form = {
        amount:'0.00',
        currency:initialState.currencies[0].code,
        postDate:dateToYMD(new Date()),
        description:'',
        wallet:'',
        category:'',
        receipt:'',
        tags:'',
        merchant:'',
      }
      state.form = form
      state.pickerSelection = null
      this.setState(state)
      return
    }


    form.amount      = formatPrice(value.value.amount)
    form.currency    = value.value.currency
    form.description = value.description

    if (value.wallet.id) {
      form.wallet = value.wallet.id
    }
    
    if (value.installment_amount) {
      form.description = `[${value.installment_occurrence}/${value.installment_total_frequency}] ${value.description}`
      form.amount      = formatPrice(value.installment_amount)
    }

    if (value.category) {
      form.category    = value.category.id
    }

    state.loan = value

    state.form = form

    state.pickerSelection = value
    this.setState(state)

    this.validateForm()
  }


  setBill(value) {
    let state = this.state
    let form  = this.state.form

    if ('bill-parent' === value) {
      state.bill = {}
      let form = {
        amount:'0.00',
        currency:initialState.currencies[0].code,
        postDate:dateToYMD(new Date()),
        description:'',
        wallet:'',
        category:'',
        receipt:'',
        tags:'',
        merchant:'',
      }
      state.form = form
      state.pickerSelection = null
      this.setState(state)
      return
    }

    form.amount      = formatPrice(value.amount)
    form.currency    = value.currency
    form.description = value.description
    form.category    = value.category

    state.bill = value
    
    state.form = form
    state.pickerSelection = value
    this.setState(state)

    this.validateForm()
  }

  morePickerComponent() {
    let component = null
    let icon = null
    let label = null

    const transactionType = this.state.transactionType

    if ('bill' === transactionType) {
      component = this.billPickerComponent()
      icon = <FontAwesome5 style={styles.rowIcon} name="money-bill-alt" size={20} />
      label = <Text style={styles.rowLabel}>Bills</Text>
    } else if ('loan' === transactionType) {
      component = this.loanPickerComponent()
      icon = <MaterialCommunityIcons style={styles.rowIcon} name="bank" size={20} />
      label = <Text style={styles.rowLabel}>Loans</Text>
    }else if ('loan-receive' === transactionType) {
      component = this.loanReceivePickerComponent()
      icon = <MaterialCommunityIcons style={styles.rowIcon} name="bank" size={20} />
      label = <Text style={styles.rowLabel}>Loans</Text>
    }

    let mainComponent = null

    if (this.state.selectedTransaction.transaction_id) {

      if ('bill' === transactionType) {
        label = <Text style={styles.rowLabel}>Bill</Text>
      } else if ('loan' === transactionType) {
        label = <Text style={styles.rowLabel}>Loan</Text>
      }else if ('loan-receive' === transactionType) {
        label = <Text style={styles.rowLabel}>Loan Received</Text>
      }

      mainComponent = 
      <View style={this.state.pickerSelection ? styles.cardPickerBody : styles.cardPickerEmptyBody } key={1}>
       <View style={styles.rowCard}>
          {icon}
          {label}
          <Text style={styles.rowValue}></Text>
        </View>
      </View>
    }else{
      mainComponent = 
      <View style={this.state.pickerSelection ? styles.cardPickerBody : styles.cardPickerEmptyBody } key={1}>
       <View style={styles.rowCard}>
          {icon}
          {label}
          <View style={styles.rowValue}>
            {component}
          </View>
          <Entypo style={styles.rowIndicator} name="chevron-right" size={20} />
        </View>
      </View>
    }

    return (mainComponent)
  }

  walletComponent() {
    return (
      <View style={styles.rowCard} key={0}>
        <Entypo style={styles.rowIcon} name="wallet" size={20} />
        <Text style={styles.rowLabel}>Wallet</Text>
        <View style={styles.rowValue}>
          {this.pickerWalletComponent()}
        </View>
        <Entypo style={styles.rowIndicator} name="chevron-right" size={20} />
      </View>
    )
  }

  categoryComponent() {
    return (
      <View style={styles.rowCard}  key={1}>
        <FontAwesome5 style={styles.rowIcon} name="shapes" size={20} />
        <Text style={styles.rowLabel}>Category</Text>
        <View style={styles.rowValue}>
          {this.pickerCategoryComponent()}
        </View>
        <Entypo style={styles.rowIndicator} name="chevron-right" size={20} />
      </View>
    )
  }

  bottomComponent() {
    let component = null
    if ('more' === this.state.formSelection && 'transfer' === this.state.transactionType) {
      component = this.transferWalletComponent()
    } else {
      component = []
      if (!this.state.selectedTransaction.transaction_id) {
        component.push(
          this.walletComponent()
        )
      }  
      component.push(
        this.categoryComponent()
      )

      if (!this.state.descriptionFocus) {
        component.push(
          this.moreDetailCardComponent()
        )
      }
    }
    return (component)
  }

  fromWalletComponent() {
    return (
      <Picker
        style={styles.pickerInputCard}
        selectedValue={this.state.transfer.fromWallet}
        onValueChange={(itemValue, itemIndex) => this.transferWallet(itemValue, 'fromWallet')}
      >
      <Picker.Item value={''}  key={1821} label={``}/>
      {this.state.wallets.map((wallet, key) => {
        return <Picker.Item value={wallet} key={wallet.wallet_id} label={`${wallet.label} - ${wallet.available_funds.currency} ${formatPrice(wallet.available_funds.value)}`}/>
      })}
      </Picker>
    )
  }

  toWalletComponent() {
    const fromWallet = this.state.transfer.fromWallet
    return (
      <Picker
        style={styles.pickerInputCard}
        selectedValue={this.state.transfer.toWallet}
        onValueChange={(itemValue, itemIndex) => this.transferWallet(itemValue, 'toWallet')}
      >
      <Picker.Item value={''}  key={1821} label={``}/>
      {this.state.wallets.map((wallet, key) => {
        if (wallet.wallet_id === fromWallet.wallet_id) {
          return true
        }  
        return <Picker.Item value={wallet} key={wallet.wallet_id} label={`${wallet.label} - ${wallet.available_funds.currency} ${formatPrice(wallet.available_funds.value)}`}/>
      })}
      </Picker>
    )
  }

  transferWallet(value, field) {
    let transfer = this.state.transfer
    const amount = cleanDecimal(this.state.form.amount)

    let state = this.state
    if ('fromWallet' === field) {
      transfer.fromWallet = value
      transfer.toWallet = {}
    }

    if ('toWallet' === field) {
      transfer.toWallet = value
    }

    state.transfer = transfer

    this.setState(state)

    this.validateForm()
  }

  showFromWallet() {
    if (this.state.screenTransfer.wallet_id) {
      return (
        <Text style={styles.rowValue}>
          {this.state.screenTransfer.label} - {this.state.screenTransfer.available_funds.currency} {formatPrice(this.state.screenTransfer.available_funds.value)}
        </Text>
      )
    } else {
      return (
        <View style={styles.rowValue}>
          {this.fromWalletComponent()}
        </View>
      )
    }
  }

  transferWalletComponent() {
    return (
      <View>
        <View style={styles.rowCard}>
          <Entypo style={styles.rowIcon} name="wallet" size={20} />
          <Text style={styles.rowLabel}>From</Text>
          {this.state.selectedTransaction.transaction_id ? (
            <Text style={styles.rowValue}>
              {this.state.transfer.fromWallet.label}
            </Text>
          ) : this.showFromWallet()}
          <Entypo style={styles.rowIndicator} name="chevron-right" size={20} />
        </View>
        {!this.state.transfer.fromWallet.wallet_id ? null : (
          <View style={styles.rowCard}>
            <Entypo style={styles.rowIcon} name="wallet" size={20} />
            <Text style={styles.rowLabel}>To</Text>
            {this.state.selectedTransaction.transaction_id ? (
              <Text style={styles.rowValue}>
                {this.state.transfer.toWallet.label}
              </Text>
            ) : (
              <View style={styles.rowValue}>
                {this.toWalletComponent()}
              </View>
            )}
            <Entypo style={styles.rowIndicator} name="chevron-right" size={20} />
          </View>
        )}
        <View style={{marginTop: 20}}>
          { !this.state.selectedTransaction.transaction_id ? (
            <Button title="TRANSFER" style={this.state.validForm ? styles.btnSaveValid : styles.btnSaveDisabled} onPress={() => this.save()} color={this.state.validForm ? '#53b3bd': '#dedede'} />
            ) : (
            <View style={{marginTop:10}}>
              <Button title="DELETE" style={{marginTop:15}} onPress={() => this.delete()} color={'#EBA0A1'} />
            </View>
          )}
        </View>
      </View>
    )
  }

  cardBodyComponent() {
    return (
      <View style={styles.cardBody} key={0}>
        <View style={styles.rowMore}>
          <Text style={{width:'100%'}} >General</Text>
        </View>
        <View style={styles.rowCard}>
          <FontAwesome style={styles.rowIcon} name="sticky-note-o" size={20} />
          <Text style={styles.rowLabel}>Description</Text>
          <View style={styles.rowValue}>
            <TextInput  
              style={styles.textInput}
              defaultValue={this.state.form.description}
              onChangeText={(inputText) => this.inputValue(inputText, 'description')}
              onBlur={(e) => this.descriptionBlur()}
              onFocus={() => this.descriptionFocus()}
            />
          </View>
        </View>
        <View style={styles.rowCard}>
          <Fontisto style={styles.rowIcon} name="date" size={20} />
          <Text style={styles.rowLabel}>Date</Text>
          <View style={styles.rowValue}>
            {this.datePickerComponent()}
          </View>
        </View>
        {this.bottomComponent()}
      </View>
    )
  }

  moreFormDetail() {
    let state = this.state
    state.moreForm = !this.state.moreForm 
    this.setState(state)
  }

  moreDetailCardComponent() {

    // <View style={styles.rowCard}>
    //   <FontAwesome style={styles.rowIcon} name="sticky-note-o" size={20} />
    //   <Text style={styles.rowLabel}>Receipt</Text>
    //   <Text style={styles.rowValue}>{previewText(this.state.form.receipt)}</Text>
    // </View>
    return (
      <View key={2}>
        <View style={styles.rowMore}>
          <Text onPress={() => this.moreFormDetail()} style={{width:'90%'}} >More Detail</Text>
          <FontAwesome5 onPress={() => this.moreFormDetail()} name="ellipsis-h" size={20}  style={{width:'10%'}} />
        </View>
        {!this.state.moreForm ? null : (
          <View >
            <View style={styles.rowCard}>
              <FontAwesome style={styles.rowIcon} name="sticky-note-o" size={20} />
              <Text style={styles.rowLabel}>Tags</Text>
              <View style={styles.rowValue}>
                <TextInput  
                  style={styles.textInput}
                  defaultValue={this.state.form.tags}
                  onChangeText={(inputText) => this.inputValue(inputText, 'tags')}
                />
              </View>
            </View>
            <View style={styles.rowCard}>
              <FontAwesome style={styles.rowIcon} name="sticky-note-o" size={20} />
              <Text style={styles.rowLabel}>Merchant</Text>
              <View style={styles.rowValue}>
                <TextInput  
                  style={styles.textInput}
                  defaultValue={this.state.form.merchant}
                  onChangeText={(inputText) => this.inputValue(inputText, 'merchant')}
                />
              </View>
            </View>
          </View>
        )}
        
        <View style={{marginTop:20}}>
          <Button title="SAVE" style={this.state.validForm ? styles.btnSaveValid : styles.btnSaveDisabled} onPress={() => this.save()} color={this.state.validForm ? '#53b3bd': '#dedede'} />
          { !this.state.selectedTransaction.transaction_id ? null : 
            (

            <View style={{marginTop:10}}>
              <Button title="DELETE" style={{marginTop:15}} onPress={() => this.delete()} color={'#EBA0A1'} />
            </View>
          )}
        </View>

      </View>
    )
  }

  bodyComponent() {
    return (
      <View style={styles.formRow}>
        {this.state.selectedTransaction.transaction_id && 'more' === this.state.formSelection ? (
          <View style={{}}>
            <Text 
            style={{
              paddingVertical: 8,
              backgroundColor: '#fff',
              borderRadius: 20,
              overflow: 'hidden', 
              textAlign: 'center',
              paddingRight:'10%',
              paddingLeft:'10%',
              marginTop:'25%'
            }}>
              {this.state.form.currency}
            </Text>
          </View>
          ) : (
          <View style={styles.currency}>
            {this.pickerCurrencyComponent()}
          </View>
        )}
        {this.state.selectedTransaction.transaction_id && 'more' === this.state.formSelection ? (
          <Text style={styles.amountInput}>
            {this.state.form.amount}
          </Text>
          ) : (
          <TextInput  
            style={styles.amountInput}  
            keyboardType={'numeric'}  
            defaultValue={this.state.form.amount}
            onChangeText={(e) => this.inputValue(e, 'amount')}
            onBlur={(e) => this.blurFormatPrice()}
            onFocus={() => this.clearAmount()}
          />
        )}
      </View>
    )
  }

  selectionComponent() {
    return (
      <View style={styles.moreBody}>
        <View style={styles.moreCard} onPress={() => this.transactionTypeChange('bill')}>
          <Text style={styles.moreLabel} onPress={() => this.transactionTypeChange('bill')}>Pay bills</Text>
          <FontAwesome5 onPress={() => this.transactionTypeChange('bill')} style={styles.moreIcon} name="money-bill-alt" size={35} />
        </View>
        <View style={styles.moreCard} onPress={() => this.transactionTypeChange('transfer')}>
          <Text style={styles.moreLabel} onPress={() => this.transactionTypeChange('transfer')}>Transfer</Text>
          <FontAwesome style={styles.moreIcon} name="exchange" size={35}  onPress={() => this.transactionTypeChange('transfer')}/>
        </View>
        <Text style={{marginTop:10}}>Debt</Text>
        <View style={styles.moreCard} onPress={() => this.transactionTypeChange('loan')}>
          <Text style={styles.moreLabel}  onPress={() => this.transactionTypeChange('loan')}>Pay debt</Text>
          <MaterialCommunityIcons style={styles.moreIcon} name="bank" size={35} onPress={() => this.transactionTypeChange('loan')}/>
        </View>
        <View style={styles.moreCard} onPress={() => this.transactionTypeChange('loan-receive')}>
          <Text style={styles.moreLabel} onPress={() => this.transactionTypeChange('loan-receive')}>Receive payment</Text>
          <FontAwesome5 style={styles.moreIcon} name="money-bill-wave-alt" size={35}  onPress={() => this.transactionTypeChange('loan-receive')}/>
        </View>
      </View>
    )
  }

  moreSelectionComponent() {
    let component = null
    if (!this.state.transactionType) {
      component = this.selectionComponent()
    }
    return (component)
  }

  moreBodySelection() {
    let component = null

    if ('bill' === this.state.transactionType && this.state.bill && this.state.bill.bill_id) {
      component = this.bodyComponent()
    }else if ('transfer' === this.state.transactionType) {
      component = this.bodyComponent()
    }else if ('loan' === this.state.transactionType && this.state.loan && this.state.loan.id) {
      component = this.bodyComponent()
    }else if ('loan-receive' === this.state.transactionType && this.state.loan && this.state.loan.id) {
      component = this.bodyComponent()
    }

    return (component)
  }

  moreBodyComponent() {
    let component = null

    if (this.state.transactionType) {

      component = []

      if ('transfer' !== this.state.transactionType) {
        component.push(
          this.morePickerComponent()
        )  
      }

      if (this.state.pickerSelection || 'transfer' === this.state.transactionType) {
        component.push(
          this.cardBodyComponent()
        )  
      }
    }

    if (this.state.amountFocus) {
      component = null
    }
    return (component)
  }

  formComponent() {
    return (
      <View>
        <View style={this.headerStyle()}>
          <View style={{width:'100%',flexDirection: 'row',position:'relative' }}>
            <View style={{width:'90%',}}></View>
            <MaterialCommunityIcons style={styles.cancelButton} name="close-circle" size={25} color={'#EBA0A1'} onPress={() => this.setModalVisible()}/>
          </View>
          <View style={styles.formRow}>
            <Text onPress={() => this.changeFormSelection('expense')} style={'expense' === this.state.formSelection ? styles.topLeftSelected : styles.topLeftSelection}>Expense</Text>
            <Text onPress={() => this.changeFormSelection('income')} style={'income' === this.state.formSelection ? styles.topSelected : styles.topSelection}>Income</Text>
            <Text onPress={() => this.changeFormSelection('more')} style={'more' === this.state.formSelection ? styles.topRightSelected : styles.topRightSelection}>More</Text>
          </View>
          {_.includes(this.state.sameFields, this.state.formSelection) ? this.bodyComponent() : this.moreBodySelection() }
        </View>
        <View style={{backgroundColor: '#f3f3f3'}}>
          {this.state.amountFocus || !_.includes(this.state.sameFields, this.state.formSelection) ? null : this.cardBodyComponent()}
          {_.includes(this.state.sameFields, this.state.formSelection) ? null : this.moreSelectionComponent()}
          {!_.includes(this.state.sameFields, this.state.formSelection) ? this.moreBodyComponent() : null}
        </View>
      </View>
    )
  }

  render() {
    return (
      <BottomModal
        visible={this.state.modalVisible}
      >
      {this.formComponent()}
      </BottomModal>
    )
  }
}

const commonStyle = StyleSheet.create(common);
const styles = StyleSheet.create(transactionForm)