const organization = {
  body:{
  },
  selector:{
    flexDirection: 'row',
    padding:10,
    width:'100%',
  },
  selected:{
    flexDirection: 'row',
    width:'100%',
    padding:10,
    backgroundColor: '#dedede',
  },
  organizationName:{
    fontWeight:'bold',
    fontSize: 15,
    width:'90%'
  }
}

export {
  organization,
}