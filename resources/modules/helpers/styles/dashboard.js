const dashboard = {
  body: {
    marginTop:-10,
    backgroundColor: '#f3f3f3',
    position:'relative',
  },
  filtersBtn:{
    fontWeight: 'bold',
    fontSize: 15, 
    marginRight:20,
    borderColor: '#dedede',
    textAlign:  'center', 
    width:"20%",
    paddingLeft:'10%',
    paddingTop:5,
    paddingBottom:5,
  },
  row:{
    marginTop:10,
    flexDirection: 'row', 
    width:'100%',
  },
  dateFilter:{
    flexDirection: 'row', 
  },
  dateFilterComponents:{
    fontSize: 20
  },
  filterDateIcon:{
    margin:5
  },
  transactions_container:{
    paddingRight:10,
    paddingLeft:10,
  },
  t_start_date:{
    flexDirection: 'row', 
    borderBottomWidth: 1,
    borderBottomColor: '#eeeeee',
  },
  dashboardSummary:{
    flexDirection: 'row', 
    // backgroundColor: '#1cd88d',
    backgroundColor: '#53b3bd',
  },
  lastRecordLabel:{
    fontWeight: 'bold',
    fontSize: 15, 
    marginRight:20,
    marginBottom: 10
  },
  sd_day:{
    fontWeight: 'bold',
    fontSize: 20, 
    marginRight:20,
  },
  sd_month:{
    marginLeft: 10
  },
  main_transaction:{
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#eeeeee', 
  },
  mt_row:{
    borderBottomWidth: 1,
    borderColor: '#dedede',
    paddingBottom: 5,
    paddingTop: 5,
  },
  mt_description:{
    width:'75%',
    paddingRight:'5%',
  },
  mt_price:{
    marginLeft:'-3%',
    width:'25%',
    flexDirection: 'row-reverse', 
    textAlign: 'right',
    position:'relative',
  },
  mt_moreDetail:{
    flexDirection: 'row',
  },
  mt_category:{
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft:5,
    marginLeft:5,
    width:'100%',
    textAlign: 'center', 
    color:'grey',
    fontSize: 12
  },
  mt_tag:{
    fontSize: 10,
    marginRight:10,
    backgroundColor: '#dedede',
  },
  mt_expense:{
    paddingBottom: 15,
    color: 'red',
    textAlign: 'right', 
    fontSize: 15,
    fontWeight: '100', 
    position:'relative',
    textAlign: 'right', 
  },
  mt_income:{
    paddingBottom: 15,
    textAlign: 'right', 
    color: 'green',
    fontSize: 15,
    fontWeight: '100', 
    position:'relative',
    textAlign: 'right', 
  },
  mt_currency_label_expense:{
    fontSize: 10,
    textAlign: 'right',
    color: 'red',
    textAlign: 'right',
    marginTop:'2%'
  },
  mt_currency_label_income:{
    fontSize: 10,
    textAlign: 'right', 
    color: 'green',
    textAlign: 'right', 
    marginTop:'2%'
  },
  cards:{
  },
  c_card_income:{
    marginTop:20,
    // backgroundColor: '#53b3bd50',
    backgroundColor: '#fff',
    padding:10,
    margin:10,
    borderRadius:15,
  },
  c_card_expense:{
    marginTop:20,
    backgroundColor: '#fff',
    // backgroundColor: '#fc822a50',
    padding:10,
    margin:10,
    borderRadius:15,
  },
  c_label_type:{
    marginTop: 5,
    textAlign: 'center',
    alignItems: 'center',  
    fontWeight: 'bold', 
  },
  c_label_type_income:{
    marginTop: 5,
    textAlign: 'center',
    alignItems: 'center',
    color:'green',  
  },
  c_label_type_expense:{
    marginTop: 5,
    textAlign: 'center',
    alignItems: 'center',
    color:'red',  
  },
  c_currency:{
    fontSize: 10,
    marginTop:3,
  },
  c_price:{
    fontSize: 30,
    fontWeight: 'bold', 
    marginTop:-5,
  },
  c_comparison:{
    textAlign: 'center',
    alignItems: 'center',  
  },
  compared_text_up:{
    color:'green',
    textAlign: 'center',
    alignItems: 'center',  
    fontWeight: 'bold',
  },
  compared_text_down:{
    color:'red',
    textAlign: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
  },
  add_icon:{
    position: 'relative', 
    left:'85%',
    bottom:'13%',
  },
  icon_bg:{
    backgroundColor: '#fff',
    width:25,
    height: 25,
    position: 'absolute',
    top:'25%',
    left:'3%',
  },
  modalContent:{
  },
  transactionCard:{
    padding:10,
    borderRadius: 15,
    backgroundColor: '#fff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
  },
}

export {
  dashboard,
}