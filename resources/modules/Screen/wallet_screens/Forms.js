
import React from 'react';
import { ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid, Alert, KeyboardAvoidingView } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";

// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign     from 'react-native-vector-icons/AntDesign';
import moment        from 'moment'
import Toast         from 'react-native-simple-toast';

import Edit        from './WalletForm'
import Transfer    from './Transfer'

const initialState = {
  'view':'edit',
  'wallet_id':'',
  'members':[],
};

export default class Forms extends React.Component {

  constructor(props){
    super(props);
    initialState.wallet_id = this.activeWallet()
    initialState.view      = 'edit'
    this.state             = initialState;
    this.transfer_child    = React.createRef();
    this.edit_child        = React.createRef();
  }

  changeView(view){
    var _this = this
    this.props.changeView(view);
  }

  activeWallet()
  {
    return this.props.activeWallet()
  }

  activeWallets()
  {
    return this.props.activeWallets()
  }

  activeWalletModel()
  {
    return this.props.activeWalletModel()
  }

  setWalletModel(wallet){
    this.props.setWalletModel(wallet);
  }

  confirmDelete(){
    Alert.alert(
      "Are you sure?",
      "You won't be able to revert this!",
      [
        { text: "Yes, delete it!", onPress: () => this.deleteTransaction() },
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
      ],
      { cancelable: false }
    );
  }

  deleteTransaction(){
    var _this = this    
    var state = _this.state
    var data = state
    var url = '/api/a/wallet/delete'
    axios.post(global.app_url+url, data).then(function(response){
      _this.changeView('home')
      Toast.show('Wallet sucessfully deleted!');
    }).catch(function(error){
      console.log(error.response.data)
      Toast.show(error.response.data);
    });   
  }


  showText ()
  {
    var _this = this
    const { navigate } = _this.props.navigation;
    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
  }

  changeType(type)
  {
    var _this = this
    var state = _this.state
    state.view = type
    _this.setState(state)
  }
  saveChanges()
  {
    var _this = this
    if (this.state.view == 'transfer') {
      this.transfer_child.current.fromParent()
    }else{
      this.edit_child.current.fromParent()
    }
  }

  render() {
    var  rs_selector =  <View style={styles.row}>
          <Text style={styles.rs_selected}  onPress={() => this.changeType('edit')}>Edit</Text>
          <Text style={styles.rs_select}  onPress={() => this.changeType('transfer')}>Transfer</Text>
        </View>
        var form = <Edit ref={this.edit_child} setWalletModel={this.setWalletModel.bind(this)}  changeView={this.changeView.bind(this)} activeWallet={this.activeWallet.bind(this)}  activeWalletModel={this.activeWalletModel.bind(this)}  />
        if (this.state.view == 'transfer') {
            form = <Transfer  ref={this.transfer_child} setWalletModel={this.setWalletModel.bind(this)} changeView={this.changeView.bind(this)} activeWallet={this.activeWallet.bind(this)} activeWallets={this.activeWallets.bind(this)} activeWalletModel={this.activeWalletModel.bind(this)} activeWalletModel={this.activeWalletModel.bind(this)} />
          rs_selector =  <View style={styles.row}>
            <Text style={styles.rs_select}  onPress={() => this.changeType('edit')}>Edit</Text>
            <Text style={styles.rs_selected} onPress={() => this.changeType('transfer')}>Transfer</Text>
          </View>
        }
    return (

      <KeyboardAvoidingView 
      behavior={"padding"}
      style={{flex:1}}
      >
      <View style={styles.body}>
        <View style={styles.row}>
            <View style={styles.back_btn}>
                <AntDesign style={styles.tbb_icon} name="back" color='black' size={15}  onPress={() => this.changeView('activities_'+this.state.wallet_id)} />
                <Text style={styles.tbb_back} onPress={() => this.changeView('activities_'+this.state.wallet_id)} >Back</Text>
            </View>
          { this.state.view == 'edit' ? (<View style={styles.delete_btn}>
              <Text style={styles.tbb_delete} onPress={() => this.confirmDelete()} >Delete</Text>
              <AntDesign style={styles.tbb_icon} name="delete" color='black' size={15}  onPress={() => this.confirmDelete()} />
          </View>) : null}
        </View>
          {rs_selector}
        <View style={styles.main_body}>
          {form}
        </View>
        <View style={styles.save_btn_view}>
          <Button title="Save" style={styles.save_add_button} onPress={() => this.saveChanges()} color={'#53b3bd'} />
        </View>
      </View>
    </KeyboardAvoidingView>
    );
  }

}


const styles = StyleSheet.create({
  main_body:{
    height: '85%',
  },
  body: {
    padding:10,
  },
  row:{
    marginTop:10,
    flexDirection: 'row', 
  },
  rs_selected:{
    width:'50%',
    textAlign: 'center',
    alignItems: 'center',
    fontSize: 15,  
    backgroundColor: '#c4eaef',
    color:'#33929c',
    paddingTop:10,
  },
  rs_select:{
    width:'50%',
    textAlign: 'center',
    alignItems: 'center',
    fontSize: 15,  
    color:'#33929c',
    paddingTop:10,
    paddingBottom: 10,
  },
  back_btn:{
    flexDirection: 'row', 
    width:'50%',
  },
  delete_btn:{
    flexDirection: 'row', 
    width:'50%',
  },
  tbb_back:{
    marginLeft:5,
  },
  tbb_delete:{
    marginLeft:'65%',
  },
  tbb_icon:{
    marginLeft:5,
    marginTop:1,
  },
  save_btn_view:{
    position:'relative',
    bottom:'11%',
  },
  save_add_button:{
    width:'50%',
  },
});
