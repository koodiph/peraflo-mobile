
import React from 'react';
import { ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid, Alert, KeyboardAvoidingView, Image, TouchableNativeFeedback } from 'react-native';
import { FontAwesome, Entypo,
         Foundation, AntDesign,
         FontAwesome5 } from 'react-native-vector-icons';
import axios         from 'axios';
import _             from 'lodash';
import { wallet, common, dashboard } from '../../helpers/styles'
import { formatPrice, capitalize, emptyObject } from '../../helpers/filters'

const initialState = {
  wallets:[],
  all:[],
  mine:[],
  notMine:{},
  unassigned:[],
  currencies:[],
  filter:{
    currency:'',
    status:'active',
  },
  pageLoader:false,
};

export default class Home extends React.Component {

  _isMounted = false;

  constructor(props){
    super(props);
    const user = global.user
    if (user) {
      const authOrganization = user.auth_organization
      initialState.currencies = authOrganization.currencies
      if (initialState.currencies.length) {
        initialState.filter.currency = initialState.currencies[0].code
      }
    }
    this.state = initialState;
  }

  componentDidMount() {
    
    this._isMounted = true;
    if (this._isMounted) {
      this.props.onRef(this);
      if (global.user) {
        global.reload_page = this.props.getParent()
        this.reloadPage()
      }
    }
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
    this._isMounted = false;
  }

  openFilter() {
    const currencies = this.state.currencies
    const filter    = this.state.filter
    const data = {
      currencies:currencies,
      filter:filter,
    }

    this.props.openFilter(data);
  }

  async reloadPage() {
    global.reload_page = this.props.getParent()
    this.pageLoader(true)
    let state = this.state
    const user = global.user

    if (user) {
      const authOrganization = user.auth_organization
      state.currencies = authOrganization.currencies
      if (state.currencies.length) {
        state.filter.currency = state.currencies[0].code
      }
      await this.setState(state)
    }

    await this.getWallets()
    this.pageLoader(false)
  }

  pageLoader(value) {
    let state = this.state
    state.pageLoader = value
    this.setState(state)
  }

  applyFilter(filter) {
    let state = this.state
    state.filter = filter
    this.setState(state)

    this.getWallets()
  }

  async getWallets() {
    const filter = this.state.filter
    const user  = global.user;
    
    const data = {
      organization_id : user.auth_organization.organization_id,
      role            : user.role.name
    }

    if (filter) {
      data.filter = filter
    }

    try {
      const response = await axios.post(global.app_url+'/api/a/wallets', data)
      let state = this.state
      const wallets = response.data.list
      const currencies = response.data.currencies

      let all = []
      let mine = []
      let notMine = {}
      let unassigned = []

      _.forOwn(wallets,function(wallet) {
        _.forOwn(wallet,function(wal){
          if (wal.name.toLowerCase().includes('system-wallet-')) {
            return true
          }
          all.push(wal)
          if (wal.user_id && wal.user.is_mine) {
            mine.push(wal)
          }else{
            const group = wal.user_id ? wal.user_id : 'none'

            if (group != 'none' && !notMine[wal.user_id]) {
                notMine[group] = {
                  user:wal.user,
                  wallets:[]
                } 
            }
    
            if (group != 'none') {
              notMine[group]['wallets'].push(wal)
            }else{
              unassigned.push(wal)
            }

          }
        })
      })
      state.all = all
      state.mine = mine
      state.notMine = notMine
      state.currencies = currencies
      state.unassigned = unassigned

      this.setState(state)
    } catch(e) {
      console.log(e)
    }
  }

  totalBalance(type) {
    const all = this.state.all
    let balance = 0

    _.forOwn(all,function(wallet){
      if (type === wallet.type) {
        balance += wallet.balance
      }
    })

    return balance
  }

  myWalletComponent() {
    let component = []
    const _this = this
    _.forOwn(this.state.mine,function(wallet, key){
      component.push(_this.listComponent(wallet, key))
    })
    return (component)
  }

  notMyWalletComponent() {
    let component = []
    const _this = this
    let walletCount = 0
    _.forOwn(this.state.notMine, function(datas,key) {
      const user = datas.user
      const wallets = datas.wallets
      let walletComponent = []
      _.forOwn(wallets,function(wallet, walletKey){
        walletComponent.push(_this.listComponent(wallet, walletKey))
      })
      component.push(
        <View key={key}>
          {
            _this.state.mine.length  !== 0 ? (
              <Text style={styles.walletLabel}>{user.display}</Text>
            ) : (
              <View style={{flexDirection: 'row', }}>
                <Text style={styles.walletLabel}>{user.display}</Text>
                {
                  walletCount === 0 ? (
                    <FontAwesome5 style={{width:'10%',}} name="ellipsis-h" color='black' size={15}  onPress={() => _this.openFilter()} />
                  ) : null
                }
              </View>
            )

          }
          {walletComponent}
        </View>
      )
      walletCount++
    })
    return (component)
  }

  selectWallet(wallet) {
    this.props.selectWallet(wallet)
  }

  transferWallet(wallet) {
    
    const transferWallet = {
      "available_funds": {
        "currency": wallet.currency,
        "value": wallet.balance,
      },
      "color": wallet.color,
      "label": wallet.name,
      "order": wallet.order,
      "owner_id": wallet.user_id,
      "starting_balance": {
        "amount": {
          "currency": wallet.currency,
          "value": wallet.starting_balance,
        },
      },
      "status": wallet.status,
      "type": wallet.type,
      "wallet_id": wallet.id,
    }

    this.props.transferWallet(transferWallet)
  }

  auditLog(wallet) {
    this.props.changeView('audit-log', wallet)
  }
  // <View style={{width:'20%',paddingLeft:'10%',paddingTop:'3%'}}>
  //   <FontAwesome5 name="ellipsis-h" color='black' size={23}  onPress={() => this.openFilter()} />
  // </View>
  listComponent(wallet, key) {
    return (
      <TouchableNativeFeedback
        key={key}
        onPress={() => this.auditLog(wallet)}
        background={TouchableNativeFeedback.Ripple('#dedede', false)}>
        <View style={styles.walletListCard}>
          <View style={{flexDirection: 'row',}}>
            <View style={{width:'50%',}}>
              <Text style={styles.cardText}>{wallet.name}</Text>
              <Text style={{color:'grey',marginBottom:1}}>{capitalize(wallet.type)}</Text>
            </View>
            <View style={styles.walletListAmount}>
              <Text style={styles.walletListAmountLabel}>
                {formatPrice(wallet.balance)}
              </Text>
              <Text style={styles.walletListCurrencyLabel}>{wallet.currency}  </Text>
            </View>
          </View>
        </View>
      </TouchableNativeFeedback>

    )
  }

  openModal() {
    this.props.openModal()
  }

  walletsComponent() {
    return (
      <ScrollView>
        <View style={commonStyle.body}>
        {this.state.mine.length ? (
            <View style={{}}>
              <View style={{flexDirection: 'row', }}>
                <Text style={styles.walletLabel}>My Wallet</Text>
                <FontAwesome5 style={{width:'10%',}} name="ellipsis-h" color='black' size={15}  onPress={() => this.openFilter()} />
              </View>
              {this.myWalletComponent()}
            </View>
          ) : null}
          {this.notMyWalletComponent()}
          {this.state.mine.length === 0 && emptyObject(this.state.notMine) ? (
              <View style={{flexDirection: 'row', }}>
                <Text style={styles.walletLabel}></Text>
                <FontAwesome5 style={{width:'10%',}} name="ellipsis-h" color='black' size={15}  onPress={() => this.openFilter()} />
              </View>
              ) : null}
        </View>
      </ScrollView>
    )
  }

  render() {
    return (
      <View>
        <View style={styles.screenHeader}>
          <Text style={styles.textLabel}>Wallets</Text>
          <View style={styles.walletButton}>
            <AntDesign style={{width:'100%',}} name="pluscircle" color='#53b3bd' size={30}  onPress={() => this.openModal()} />
          </View>
        </View>
        <View style={styles.walletContainer}>
          <View style={styles.headerSection}>
            <View style={styles.headerCard}>
              <Text style={{marginTop: 5,textAlign: 'center',alignItems: 'center', fontWeight: 'bold', }}>Cash</Text>
              <Text style={{}}>{this.state.filter.currency}</Text>
              <Text style={{fontSize: 30,fontWeight: 'bold',}}>{formatPrice(this.totalBalance('cash'))}</Text>
            </View>
            <View style={styles.headerCard}>
              <Text style={{marginTop: 5,textAlign: 'center',alignItems: 'center', fontWeight: 'bold', }}>Credit Balance</Text>
              <Text style={{}}>{this.state.filter.currency}</Text>
              <Text style={{fontSize: 30,fontWeight: 'bold', }}>{formatPrice(this.totalBalance('credit card'))}</Text>
            </View>
          </View>
        {this.state.pageLoader ? (
          <Image style={{ width: 100, height: 50,marginLeft:'39%',marginTop:'10%', resizeMode: 'contain' }} source={require('../../../../assets/Spinner-0.3s-58px.gif')} />
        ) : this.walletsComponent()}
        </View>
      </View>
    )
  }
}

const commonStyle = StyleSheet.create(common);
const styles = StyleSheet.create(wallet)
const dashboardStyles = StyleSheet.create(dashboard)
