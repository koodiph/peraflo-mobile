
import React from 'react';
import { Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid, AsyncStorage  } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import Toast                    from 'react-native-simple-toast';
// import styles from '../../../resources/styles/main.js';
import axios  from 'axios';
import Config from "react-native-config";

const initialState = {
    email:'',
    password:'',
    loader:false,
    tokenExist:true,
};

const registrationPage = '/register'
// const initial_environment = 'http://app.accounting.dev8.koodi.ph' 
// const initial_environment = 'http://192.168.1.12:8080' 
const initial_environment = 'https://app.peraflo.com' 
export default class Login extends React.Component {

  constructor(props){
    super(props);
    this.state= initialState;
    global.app_url = initial_environment
    global.user = null
    global.navigation = this
  }

  componentDidMount() {
    // this.checkLogin()
  }

  navigationProp(navigate_link){
    const { navigate } = this.props.navigation;
    navigate(navigate_link);
  }

  setLoader(value) {
    let state = this.state
    state.loader = value
    this.setState(state)
  }

  tokenExist(value) {
    let state = this.state
    state.tokenExist = value
    this.setState(state)
  }

  async checkLogin(){

    try {
      const response = await AsyncStorage.getItem('user_logged')
      const response_data = JSON.parse(response)
      if (response_data) {
        axios.defaults.headers.common = {'Authorization': `Bearer `+response_data.access_token}
      }
      await this.checkConnection()
      if (this.state.tokenExist) {
        this.render()
      }else{
        global.user = response_data
        const { navigate } = this.props.navigation;
        navigate('Core');
      }

    }catch(e){
      Toast.show(e);
    }
  }

  async checkConnection() {
    const url = global.app_url+'/api/a/connection'
    try {
      const response = await axios.post(url, global.user)
      if (response.data.guest !== undefined) {
        this.tokenExist(response.data.guest)
      }else{
        this.tokenExist(true)
      }
    }catch(e){
      return true
    }
  }

  async logIn ()
  {
    const { navigate } = this.props.navigation;

    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

    const data = {
        email: this.state.email,
        password: this.state.password,
    };

    if (!data.email || !data.password) {
      Toast.show('Please fill up email and password to login');
      return;
    }
    const url = global.app_url+'/api/a/login'
    const loader = this.state.loader
    if (!loader) {
      this.setLoader(true)
      try {
        const response = await axios.post(url, data)
        this.setLoader(false)

        if (response.data.auth_organization) {
          global.user = response.data
        }else{
          response.data.auth_organization = response.data.organizations[0]
          global.user = response.data
        }

        const state = initialState

        if (response.data.auth_organization) {
          AsyncStorage.setItem('user_logged', JSON.stringify(response.data))
        }else{
          AsyncStorage.setItem('user_logged', JSON.stringify(response.data))
        }
        axios.defaults.headers.common = {'Authorization': `Bearer `+response.data.access_token}
        this.setState(state)
        navigate('Core');
      }catch(e){
        console.log(e.response.data)
        this.setLoader(false)
        Toast.show('Unable to log in.');
      }
    }else{
      Toast.show('Currently processing login.');
    }
  }


  goToPage(){
    Linking.canOpenURL(global.app_url + registrationPage).then(supported => {
      if (supported) {
        Linking.openURL(global.app_url + registrationPage);
      } else {
        console.log("Don't know how to open URI: " + global.app_url + registrationPage);
      }
    });
  }

  render() {
    return (
      <View style={styles.body}>
        <View>
          <Text style={styles.headerText}>Login</Text>
          <TextInput
            style={styles.emailInput}
            placeholder="Email Address"
            onChangeText={(email) => this.setState({email})}
            value={this.state.email}
          />
          <TextInput
            style={styles.textStyle}
            placeholder="Password"
            name="password"
            secureTextEntry={true}
            onChangeText={(password) => this.setState({password})}
            value={this.state.password}
          />
          <Button
            style={styles.btnSignin}
            title="LogIn"
            color={this.state.loader ?  "#dedede" : "#53b3bd"}
            onPress={() => this.logIn()}
          />
          <View style={{marginTop: 10}}>
            <Text style={styles.btnRegister} onPress={() => this.goToPage()}>REGISTER</Text>
          </View>
       </View>
      </View>
    );
  }
}



const styles = StyleSheet.create({
  btnSignin:{
    marginTop:50,
    marginBottom:20,
    borderRadius: 100,
    borderWidth: 1,
  },
  btnRegister:{
    textAlign: 'center', 
    color: '#53b3bd'
  },
  headerText:{
    fontWeight:'bold',
    marginBottom:20,
    width:200,
    textAlign:"center",
    color:Colors.black,
  },
  body: {
   flex: 1, alignItems: 'center', justifyContent: 'center',backgroundColor:'#f0f0f0', 
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  emailInput:{
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    width:200,
    padding:10,
    backgroundColor:'white',
  },
  textStyle:{
    borderRadius: 4,
    borderWidth: 0.5,
    borderColor: '#d6d7da',
    marginBottom:20,
    width:200,
    padding:10,
    marginTop:5,
    backgroundColor:'white',
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});
