import React from 'react';
import { ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid, Dimensions, Image } from 'react-native';
import { common, wallet,
         report, dashboard } from '../helpers/styles'
import { dateToYMD, monthYear, formatPrice } from '../helpers/filters'
import { FontAwesome5 } from 'react-native-vector-icons';
import { LineChart, Grid, YAxis } from 'react-native-svg-charts'
import * as shape from 'd3-shape'

import moment from 'moment'
import axios  from 'axios'
import _  from 'lodash'

const initialState = {
  currencies:[],
  filter:{
    date:'',
    currency:'',
  }, 
  report:{
    income:[],
    expense:[],
    categories:{}
  },
  pageLoader:false,
  cardLoader:false,
};

const screenWidth = Dimensions.get("window").width;

const data = {
  labels: ["January", "February", "March", "April", "May", "June"],
  datasets: [
    {
      data: [20, 45, 28, 80, 99, 43],
      color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
      strokeWidth: 2 // optional
    }
  ],
  legend: ["Rainy Days"] // optional
};

export default class Report extends React.Component {


  constructor(props){
    super(props);
    const presentDate = new Date();
    
    if (global.user == null) {
      this.state = initialState;
      this.props.navigationProp('Login')
      return
    }

    initialState.filter.date = dateToYMD(new Date())
    initialState.currencies  = global.user.auth_organization.currencies

    if (initialState.currencies.length) {
      initialState.filter.currency = initialState.currencies[0].code
    }

    this.state  = initialState;
  }


  componentDidMount() {

    this._isMounted = true;

    if (this._isMounted) {
      this.props.onRef(this);
      if (global.user) {
        this.reloadPage()
      }
    }

  }

  async reloadPage() {
    global.reload_page = this
    this.pageLoader(true)
    await this.getReport()
    this.pageLoader(false)
  }

  pageLoader(value) {
    let state = this.state
    state.pageLoader = value
    this.setState(state)
  }

  cardLoader(value) {
    let state = this.state
    state.cardLoader = value
    this.setState(state)
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  async getReport() {
    const filter = this.state.filter
    const organization = global.user.auth_organization
    filter.organization_id = organization.organization_id
    filter.role = global.user.role.name

    if (!filter.organization_id) {
      filter.organization_id = organization.id
    }

    try {
      this.cardLoader(true)

      const response = await axios.post(global.app_url+'/api/a/report/data', filter)
      let state = this.state

      state.report = response.data
      this.setState(state)
      
      this.cardLoader(false)

    } catch(e) {
      console.log(e)
      this.cardLoader(false)
    }

  }

  // filter date start
  monthProceed(action) {
    let date = null
    if (action == 'next') {
      date = moment(String(this.state.filter.date)).add(1, 'M').format('MMMM DD, Y')
    }else if (action == 'prev') {
      date = moment(String(this.state.filter.date)).add(-1, 'M').format('MMMM DD, Y')
    }
    let state = this.state
    state.filter.date = dateToYMD(new Date(date))
    this.setState(state)
    this.getReport()
  }
  // filter date end

  transactionComponents(datas) {
    let transactions = []
    _.forOwn(datas.transactions,function(transaction,key){
      transactions.push(
        <View style={{flexDirection: 'row', borderTopWidth: 1,borderTopColor: '#f3f3f3',paddingTop: 10,paddingBottom: 10}} key={key}>
          <Text style={{width:'65%'}}>{transaction.description}</Text>
          <Text style={{width:'35%',textAlign: 'right' }}>{formatPrice(transaction.amount)}</Text>
        </View>
      )
    })

    transactions.push(
      <View style={{flexDirection: 'row', borderTopWidth: 1,borderTopColor: '#f3f3f3',paddingTop: 10,paddingBottom: 10}} key={123}>
        <Text style={{width:'65%',fontWeight: 'bold'}}>Total Amount:</Text>
        <Text style={{width:'35%',textAlign: 'right' }}>{formatPrice(datas.total)}</Text>
      </View>
    )

    return (transactions)
  }

  categoryTitle(datas, key, category) {
    return (
      <View style={{padding:10,}} key={key}>
        <View style={dashboardStyle.transactionCard}>
          <Text style={{fontWeight: 'bold',}}>{category}</Text>
          {this.transactionComponents(datas)}
        </View>
      </View>
    )
  }

  reportBodyComponent() {
    let categoryComponent = []
    const _this = this
    let key = 0
    _.forOwn(this.state.report.categories,function(datas,category){
      categoryComponent.push(
        _this.categoryTitle(datas, key, category)
      )
      key++
    })

    return ( categoryComponent )
  }

  transactionBody() {
    const data1 = this.state.report.expense
    const data2 = this.state.report.income

    const data = [
        {
            data: data1,
            svg: { stroke: 'red' },
        },
        {
            data: data2,
            svg: { stroke: '#53b3bd' },
        },
    ]

    const minMax = { top: 30, bottom: 30 }

    return (
      <View>
        <View style={{padding:10,}}>
          <View style={dashboardStyle.transactionCard}>
            <View  style={{ height: 200, flexDirection: 'row',position:'relative' }}>
                <YAxis
                    data={data2}
                    contentInset={minMax}
                    svg={{
                        fill: 'grey',
                        fontSize: 10,
                    }}
                    numberOfTicks={10}
                    formatLabel={(value) => `${formatPrice(value)}`}
                />
                <LineChart
                    style={{ flex: 1, marginLeft: 16,width:'100%', }}
                    data={data}
                    contentInset={minMax}
                    curve={shape.curveNatural}
                >
                    <Grid />
                </LineChart>
            </View>
          </View>
        </View>
        {this.state.cardLoader ? (
          <Image style={{ width: 100, height: 50,marginLeft:'38%',marginTop:'10%', resizeMode: 'contain' }} source={require('../../../assets/Spinner-0.3s-58px.gif')} />
        ) : this.reportBodyComponent()}
      </View>
    )
  }

  render() {

    return (
      <View style={{}}>
        <View style={walletStyle.screenHeader}>
          <Text style={walletStyle.textLabel}>Report</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom:'10%'}}>
          <View style={{padding:10,}}>
            <View style={dashboardStyle.transactionCard}>
              <View style={{flexDirection: 'row'}}>
                <View style={{flexDirection: 'row',width:'80%',marginLeft:5, marginTop:5,marginBottom:5,}}>
                    <FontAwesome5 onPress={() => {
                    this.monthProceed('prev')
                  }} name="angle-double-left" color='black' size={20} />
                    <Text style={{marginLeft:20,marginRight:20,}}>
                      {monthYear(this.state.filter.date)}
                    </Text>
                    <FontAwesome5 onPress={() => {
                    this.monthProceed('next')
                  }} name="angle-double-right" color='black' size={20} />
                </View>
              </View>
            </View>
          </View>

          {this.state.pageLoader ? (
            <Image style={{ width: 100, height: 50,marginLeft:'38%',marginTop:'10%', resizeMode: 'contain' }} source={require('../../../assets/Spinner-0.3s-58px.gif')} />
          ) : this.transactionBody()}
        </ScrollView>
      </View>
    )
  }
}



const styles = StyleSheet.create(report);
const walletStyle = StyleSheet.create(wallet)
const dashboardStyle = StyleSheet.create(dashboard)
const commonStyle = StyleSheet.create(common);
