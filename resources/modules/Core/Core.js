
import React from 'react';
import { Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid, AsyncStorage } from 'react-native';
import Modal from 'react-native-modal';

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer }              from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { MaterialIcons, Entypo, FontAwesome} from 'react-native-vector-icons';

import axios  from 'axios';
import Config from "react-native-config";

// screens
import Dashboard       from '../Screen/Dashboard';
import Wallet          from '../Screen/Wallet';
import Setting         from '../Screen/Setting';
import Report          from '../Screen/Report';
import TransactionForm from './component/TransactionForm';
import WalletFilter    from './component/WalletFilter';
import WalletForm    from './component/WalletForm';
import { ModalPortal } from 'react-native-modals';

const FormComponent = () => {
  return null
}

export default class Core extends React.Component {

  _currentTab = 'dashboard'

  constructor(props){
    super(props);
  }

  navigationProp(navigateLink){
    const { navigate } = this.props.navigation;
    navigate(navigateLink);
  }

  mainNavitionProp(header)
  {
    const { navigate } = this.props.navigation;
    // this.props.navigation.setParams({headerLeft:header})
  }

  componentDidMount() {
    this.checkLogin()
  }

  checkLogin(){
    AsyncStorage.getItem('user_logged').then(function(response){
      const response_data = JSON.parse(response)
      if (response_data && response_data.access_token) {
        axios.defaults.headers.common = {'Authorization': `Bearer `+response_data.access_token}
        global.user = response_data
      }else{
        const { navigate } = this.props.navigation;
        navigate('Login');
      }
    }).catch(function(error){
      console.log(error)
    })
  }

  async addForm() {
    await this.form.setModalVisible()
  }

  async loadTransactions() {
    await this.dashboardScreen.transactions()
  }

  selectTransaction(transaction) {
    this.form.selectedTransaction(transaction)
  }

  openWalletFilter(data) {
    this.walletFilter.setModalVisible(data)
  }

  filterWallet() {
    const filter = this.walletFilter.state.filter 
    this.walletScreen.applyFilter(filter)
  }

  selectWallet(wallet) {
    this.walletForm.selectWallet(wallet)
  }

  async openModal() {
    await this.walletForm.setModalVisible()
  }

  transferWallet(wallet) {
    this.form.transferWalletFromWalletScreen(wallet)
  }

  reloadWallet() {
    this.walletScreen.reloadWallet()
  }

  render() {
    let _this = this
    const Tab = createMaterialBottomTabNavigator();
    const tabtheme = {
      colors: {
        primary: '#fff',
      },
    };

    const styles = StyleSheet.create({
      navDesign:{
        backgroundColor: 'white',
      },
    });


    const SettingScreen = () => <Setting 
                              navigationProp={this.navigationProp.bind(this)} 
                              mainNavitionProp={this.mainNavitionProp.bind(this)} 
                              />
    const DashboardScreen = () => <Dashboard 
                                onRef={ref => (this.dashboardScreen = ref)}
                                navigationProp={this.navigationProp.bind(this)}
                                selectTransaction={this.selectTransaction.bind(this)}
                                mainNavitionProp={this.mainNavitionProp.bind(this)} 
                                />
    const WalletScreen = () => <Wallet
                              onRef={ref => (this.walletScreen = ref)}
                              navigationProp={this.navigationProp.bind(this)}
                              openWalletFilter={this.openWalletFilter.bind(this)}
                              openModal={this.openModal.bind(this)}
                              selectWallet={this.selectWallet.bind(this)}
                              mainNavitionProp={this.mainNavitionProp.bind(this)} 
                              transferWallet={this.transferWallet.bind(this)} 
                              />
    const BudgetScreen = () => <Budget
                              navigationProp={this.navigationProp.bind(this)}
                              mainNavitionProp={this.mainNavitionProp.bind(this)} 
                              />
    const ReportScreen = () => <Report
                              onRef={ref => (this.reportScreen = ref)}
                              navigationProp={this.navigationProp.bind(this)}
                              mainNavitionProp={this.mainNavitionProp.bind(this)} 
                              />
    const navigation =
    <NavigationContainer theme={tabtheme}>
        <Tab.Navigator style={styles.navDesign} >
          <Tab.Screen 
            options={{
              tabBarLabel: 'Home',
              tabBarIcon: ({ color }) => (
                <MaterialIcons name="home" color={color} size={26} />
              )
            }}
            name="Home"
            listeners={() => ({
              tabPress: (e) => {
                _this.dashboardScreen.transactions()
              },
            })}
            component={DashboardScreen} />
          <Tab.Screen
            options={{
              tabBarLabel: 'Wallet',
              tabBarIcon: ({ color }) => (
                <Entypo name="wallet" color={color} size={26} />
              ),
            }}
            name="Wallet"
            listeners={() => ({
              tabPress: (e) => {
                if (_this.walletScreen) {
                  _this.walletScreen.reloadPage()
                }
              },
            })}
            component={WalletScreen} />
          <Tab.Screen
            options={{
              tabBarLabel: 'Form',
              animationEnabled: true,
              tabBarIcon: ({ color }) => (
                <Entypo name="circle-with-plus" color={'#53b3bd'} size={26} />
              ),
            }}
            component={FormComponent}
            name="Form"
            listeners={() => ({
              tabPress: (e) => {
                e.preventDefault();
                _this.addForm()
              },
            })}
           />
          <Tab.Screen
            options={{
              tabBarLabel: 'Report',
              tabBarIcon: ({ color }) => (
                <FontAwesome name="bar-chart" color={color} size={26} />
              ),
            }}
            name="Report"
            listeners={() => ({
              tabPress: (e) => {
                if (_this.reportScreen) {
                  _this.reportScreen.reloadPage()
                }
              },
            })}
            component={ReportScreen} />
          <Tab.Screen
            options={{
              tabBarLabel: 'Settings',
              tabBarIcon: ({ color }) => (
                <MaterialIcons name="account-circle" color={color} size={26} />
              ),
            }}
             name="Setting"
             component={SettingScreen} >
            </Tab.Screen>
        </Tab.Navigator>
        <TransactionForm onRef={ref => (this.form = ref)} loadTransactions={this.loadTransactions.bind(this)} reloadWallet={this.reloadWallet.bind(this)}  />
        <WalletForm  onRef={ref => (this.walletForm = ref)} filterWallet={this.filterWallet.bind(this)} />
        <WalletFilter onRef={ref => (this.walletFilter = ref)} filterWallet={this.filterWallet.bind(this)} openWalletFilter={this.openWalletFilter.bind(this)} />
        <ModalPortal />
      </NavigationContainer>
    return (navigation);
  }
}