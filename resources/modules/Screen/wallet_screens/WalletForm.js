
import React from 'react';
import { Platform, ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid, Picker, KeyboardAvoidingView } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";

// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign     from 'react-native-vector-icons/AntDesign';
import moment        from 'moment'
import Toast         from 'react-native-simple-toast';

const initialState = {
  'view':'edit',
  'auth_organization':{},
  'wallet':{
    'currency':'PHP',
    'organization_id':'',
    'user_id':'',
    'name':'',
    'current_amount':'',
    'status':'active',
    'color':'default',
    'created_by':'',
    'wallet':[],
  },
  members:[],
  wallet_id:'',
  auth_id:'',
  deleted_pockets:[],
};

export default class WalletForm extends React.Component {

  constructor(props){
    super(props);

    initialState.wallet_id               = this.activeWallet()
    initialState.auth_organization       = global.user.auth_organization
    initialState.auth_id                 = global.user.id
    initialState.members                 = global.user.auth_organization.members
    if (initialState.wallet_id) {
      var wallet = this.activeWalletModel()
      var pockets = [];
      var pocket = wallet.wallet_pockets.map((w, k) =>{
        pockets.push(w)
      })
      wallet.wallet       = pockets
      initialState.wallet = wallet
    }else{
      initialState.wallet.user_id          = global.user.id
      initialState.wallet.created_by       = global.user.id
      initialState.wallet.organization_id  = global.user.auth_organization.id
      initialState.wallet.currency         = global.user.auth_organization.settings.currency[0].code
    }
    this.state             = initialState;
    global.reload_page     = this
  }

  componentDidMount() {
    var _this = this
    var initialState = {
      'view':'edit',
      'auth_organization':{},
      'wallet':{
        'currency':'PHP',
        'organization_id':'',
        'user_id':'',
        'name':'',
        'current_amount':'',
        'status':'active',
        'color':'default',
        'created_by':'',
        'wallet':[],
      },
      members:[],
      wallet_id:'',
      auth_id:'',
      deleted_pockets:[],
    };

    initialState.wallet_id               = this.activeWallet()
    initialState.auth_organization       = global.user.auth_organization
    initialState.auth_id                 = global.user.id
    initialState.members                 = global.user.auth_organization.members
    if (initialState.wallet_id) {
      var wallet = this.activeWalletModel()
      var pockets = [];
      var pocket = wallet.wallet_pockets.map((w, k) =>{
        pockets.push(w)
      })
      wallet.wallet       = pockets
      initialState.wallet = wallet
    }else{
      initialState.wallet.created_by       = global.user.id
      initialState.wallet.organization_id  = global.user.auth_organization.id
      initialState.wallet.user_id          = global.user.id
      initialState.wallet.currency         = global.user.auth_organization.settings.currency[0].code 
    }
    _this.setState(initialState)
  }

  reloadPage()
  {
    this.changeView('home')
  }

  activeWallet()
  {
    return this.props.activeWallet()
  }

  activeWalletModel()
  {
    return this.props.activeWalletModel()
  }

  showText ()
  {
    var _this = this
    const { navigate } = _this.props.navigation;
    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
  }

  setSelecteOption(selected, field){
    var _this = this
    var state = _this.state
    state.wallet[field] = selected
    _this.setState(state)
  }

  setWalletModel(wallet){
    this.props.setWalletModel(wallet);
  }

  changeView(view){
    this.props.changeView(view);
  }

  fromParent(){
    this.saveChanges()
  }

  addPocket()
  {
    var _this = this
    var state = _this.state
    state.wallet.wallet.push({
      name:'',
      amount:'',
    })
    _this.setState(state)
  }

  removePocket(key){
    var _this = this
    var state = _this.state
    if (state.wallet_id) {
      var wallet = state.wallet.wallet[key]
      if (wallet.id) {
        var wallet_id = wallet.id
        state.deleted_pockets.push(wallet_id) 
      }
    }
    
    state.wallet.wallet.splice(key, 1)
    _this.setState(state)
  }

  pocketChange(value, field, key){
    var _this = this
    var state = _this.state
    var pockets = state.wallet.wallet
    pockets[key][field] = value
    state.wallet.wallet = pockets
    _this.setState(state)
  }

  saveChanges(){
    var _this = this
    var state = _this.state
    var validate_field = true
    var data  = state.wallet
    var url = '/api/a/wallet/store'
    if (_this.state.wallet_id) {
       url = '/api/a/wallet/update'
       data.deleted_pockets = state.deleted_pockets
    }

    var state = _this.state
    if (
      !data.currency ||
      !data.user_id ||
      !data.name ||
      !data.current_amount
    ) {
      validate_field = false
    }
    if (!validate_field) {
      Toast.show('Fill up required fields');
      return;
    }
    if (validate_field) {
        axios.post(global.app_url+url, data).then(function(response){
          var wallet= {
            'currency':'PHP',
            'organization_id':'',
            'user_id':'',
            'name':'',
            'current_amount':'',
            'status':'active',
            'color':'default',
            'created_by':'',
            'wallet':[],
          }
          var state = _this.state
          state.wallet = wallet
          _this.setState(state)
          
          if (_this.state.wallet_id) {
            _this.setWalletModel(response.data)
            _this.changeView('activities_'+_this.state.wallet_id)
          }else{
            _this.changeView('home')
            Toast.show('Wallet sucessfully added!'); 
          }
        }).catch(function(error){
          // Toast.show(error.response.data);
        });   
      }
  }

  render() {
    let membersItem = this.state.members.map( (member, key) => {
        return <Picker.Item key={key} value={member.id} label={member.first_name+' '+member.last_name+' ('+member.email+')'} />
    });


    let currencyItems = this.state.auth_organization.settings.currency.map( (currency, key) => {
        return <Picker.Item key={key} value={currency.code} label={currency.code} />
    });
    let pockets = this.state.wallet.wallet.map((pocket,key) => {
        return  <View key={key} style={styles.pocket_rows}>
                  <View style={styles.pocket_column}>
                    <Text >Name</Text>
                    <TextInput  
                      style={styles.pocket_column_text}  
                      defaultValue={pocket.name}
                      onChangeText={(inputText) => this.pocketChange(inputText,'name',key)}
                    />
                  </View>
                  <View style={styles.pocket_column}>
                    <Text >Amount ({this.state.wallet.currency})</Text>
                    <TextInput  
                      defaultValue={''+pocket.amount}
                      onChangeText={(inputText) => this.pocketChange(inputText,'amount',key)}
                      style={styles.pocket_column_text}  
                      keyboardType={'numeric'}  
                    />  
                  </View>
                  <View style={styles.pocket_close}>
                    <MaterialIcons onPress={() => this.removePocket(key)}  name="close" color='red' size={25} style={{marginTop:30,marginLeft:20}} />
                  </View>
              </View>
    });
    return (
      <KeyboardAvoidingView 
      behavior={this.state.wallet_id ? null : "padding"}
      style={{flex:1}}
      >
      <View style={styles.body}>
      {!this.state.wallet_id ? (
        <View style={styles.row_create}>
            <AntDesign style={{marginRight:10}} name="back" color='black' size={15}  onPress={() => this.changeView('home')} />
            <Text  onPress={() => this.changeView('home')} >Back</Text>
        </View>) : null}
        <ScrollView style={styles.form_input}>
          <View >
            <Text style={styles.fi_label}>Member <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_row}>
              <View
              style={styles.fi_half_picker}
              >
                <Picker
                  selectedValue={this.state.wallet.user_id}
                  onValueChange={(itemValue, itemIndex) => this.setSelecteOption(itemValue,'user_id')}
                >
                {membersItem}
                </Picker>
              </View>
            </View>
            <Text style={styles.fi_label}>Wallet Name <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_row}>
                <TextInput  
                  style={styles.fi_input_full_width}  
                  defaultValue={this.state.wallet.name}
                  onChangeText={(inputText) => this.setSelecteOption(inputText,'name')}
                />  
            </View>
            <Text style={styles.fi_label}>Amount <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_row}>
              <View
                style={styles.fi_picker}
                >
                <Picker
                  selectedValue={this.state.wallet.currency}
                  onValueChange={(itemValue, itemIndex) => this.setSelecteOption(itemValue,'currency')}
                >
                 {currencyItems}
                </Picker>
              </View>
                <TextInput  
                  style={styles.fi_textinput}  
                  keyboardType={'numeric'}  
                  defaultValue={''+this.state.wallet.current_amount}
                  onChangeText={(inputText) => this.setSelecteOption(inputText,'current_amount')}
                />  
            </View>
            <Text style={styles.fi_label}>Status <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_row}>
              <View
                style={styles.fi_picker_full_width}
                >
                <Picker
                  selectedValue={this.state.wallet.status}
                  onValueChange={(itemValue, itemIndex) => this.setSelecteOption(itemValue,'status')}
                >
                  <Picker.Item label="Active" value="active" />
                  <Picker.Item label="inactive" value="inactive" />
                </Picker>
              </View>
            </View>
            <View style={styles.pocket_section}>
              <View>
                <Button title="+ Add Pocket" onPress={() => this.addPocket()} style={styles.pocket_add_button} color={'#53b3bd'} />
                {pockets}
              </View>
            </View>
          </View>
        </ScrollView>
        {this.state.wallet_id ? null : (
        <View style={styles.btn_width}>
            <Button title="Save" style={styles.save_add_button} onPress={() => this.saveChanges()} color={'#53b3bd'} />
        </View>)}
      </View>
      </KeyboardAvoidingView>
    );
  }

}


const styles = StyleSheet.create({
  body: {
    padding:10,
    marginBottom: 10,
    backgroundColor: '#fff'
  },
  pocket_section:{
    marginTop:10,
    paddingTop:10,
    borderTopColor: '#dedede',
    borderTopWidth:0.5,
    marginBottom:25,
  },
  pocket_add_button:{
  },
  pocket_rows:{
    flexDirection: 'row',
    width:'100%', 
    marginTop:5,
  },
  pocket_column:{
    fontSize:15,
    width:'42.5%',
    padding:10,
  },
  pocket_close:{
    fontSize:15,
    width:'15%',
    paddingTop: 10,
  },
  row:{
    flexDirection: 'row', 
  },
  row_create:{
    flexDirection: 'row', 
    marginBottom: 10,
  },
  pocket_column_text:{
    marginTop:5,
    borderWidth: 1,
    borderColor: '#dedede',
    width:'100%',
    marginLeft:5,
    padding:5,
  },
  form_input:{
    height: '87%',
    borderWidth: 1,
    borderColor:'#c4eaef',
    padding:10,
  },
  fi_row:{
    flexDirection: 'row', 
    marginTop: 10,
    marginBottom: 10,
  },
  fi_picker:{
    height: 50,
    marginRight: 20,
    width: '30%',
    borderWidth: 1,
    borderColor: '#dedede',
  },
  fi_picker_full_width:{
    width: '100%',
    borderWidth: 1,
    borderColor: '#dedede',
  },
  fi_textinput:{
    borderWidth: 1,
    borderColor: '#dedede',
    width: '65%',
    height: 50,
    padding:5,
    marginTop:0,
  },
  fi_label:{
  },
  fi_input_full_width:{
    borderWidth: 1,
    borderColor: '#dedede',
    width: '100%',
    height: 50,
    padding:5,
  },
  fi_input_half_width:{
    borderWidth: 1,
    borderColor: '#dedede',
    width: '75%',
    height: 35,
    padding:5,
  },
  fi_r_date_label:{
    width: '25%',
    marginTop: 6,
  },
  fi_date_picker:{
    width: '75%',
  },
  fi_half_picker:{
    height: 50,
    width: '100%',
    borderWidth: 1,
    borderColor: '#dedede',
  },
  fi_r_label:{
    width: '35%',
    marginTop: 13,
  },
  save_add_button:{
    width:'70%',
    marginTop:10,
  },
  btn_width:{
    width:'100%',
    padding:10,
  },
  btn_width_mgbtm:{
    width:'100%',
  }
});
