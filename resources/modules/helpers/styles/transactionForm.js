const transactionForm = {
  body: {
    padding:10,
  },
  formRow:{
    flexDirection: 'row',
    marginTop:10,
  },
  formHeaderExpenseRow:{
    backgroundColor: '#fed8bd',
    padding:20,
  },
  formHeaderIncomeRow:{
    backgroundColor: '#91E699',
    padding:20,
  },
  formHeaderMoreRow:{
    backgroundColor: '#f3f3f3',
    padding:20,
  },
  topSelection:{
    width: '33%',
    textAlign: 'center',
    backgroundColor: '#dedede',
    paddingTop:10,
    paddingBottom:10,
    opacity: 0.65,
  },
  topSelected:{
    width: '33%',
    textAlign: 'center',
    color:'#ffffff',
    backgroundColor: '#53b3bd',
    paddingTop:10,
    paddingBottom:10,
  },
  topLeftSelected:{
    width: '33%',
    textAlign: 'center',
    color:'#ffffff',
    backgroundColor: '#53b3bd',
    paddingTop:10,
    paddingBottom:10,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
  },
  topLeftSelection:{
    width: '33%',
    textAlign: 'center',
    backgroundColor: '#dedede',
    paddingTop:10,
    paddingBottom:10,
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    opacity: 0.65,
  },
  topRightSelected:{
    width: '33%',
    textAlign: 'center',
    color:'#ffffff',
    backgroundColor: '#53b3bd',
    paddingTop:10,
    paddingBottom:10,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
  },
  topRightSelection:{
    width: '33%',
    textAlign: 'center',
    backgroundColor: '#dedede',
    paddingTop:10,
    paddingBottom:10,
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    opacity: 0.65,
  },
  currency:{
    marginTop:20,
    width:'27%',
  },
  amount:{
    fontSize: 50,
    position:'absolute',
    right:0,
    top:'10%',
  },
  headerField:{
    fontWeight: 'bold', 
  },
  textInput:{
    width:'100%',
  },
  buttonSaveField: {
    marginTop:10,
    borderWidth: 1,
    borderColor: '#53b3bd',
    backgroundColor: '#53b3bd',
    padding:5,
    alignSelf: 'flex-end',
  },
  buttonCancelField: {
    marginTop:10,
    borderWidth: 1,
    borderColor: 'red',
    backgroundColor: '#fc822a50',
    padding:5,
    alignSelf: 'flex-start',
  },
  viewButtons: {
    width:'50%',
  },
  pickerInput: {
    borderWidth: 1,
    borderColor: '#f3f3f3',
    fontSize: 1,
  },
  cardForm:{
    borderColor: '#dedede',
    borderWidth: 2,
  },
  cardBody:{
    padding:20,
    paddingTop: 0,
  },
  cardPickerBody:{
    padding:20,
    paddingBottom: 0,
  },
  cardPickerEmptyBody:{
    padding:20,
    paddingBottom: 100,
  },
  rowCard: {
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    width:'110%',
    marginLeft:'-5%',
    paddingLeft: '5%',
    paddingRight: '5%',
    paddingTop:'2%',
    paddingBottom:'2%',
    borderBottomWidth: 0.5,
    borderBottomColor: '#dedede',
  },
  rowIcon:{
    width:'10%',
  },
  rowLabel:{
    width:'35%',
  },
  rowValue:{
    width:'50%',
  },
  rowIndicator:{
    width:'5%',
  },
  rowMore: {
    marginTop:10,
    paddingBottom:10,
    flexDirection: 'row', 
    width:'100%',
    borderBottomColor: '#dedede',
    borderBottomWidth: 0.5,
  },
  dateInput: {
    width:'100%',
    borderWidth: 1,
    borderColor: '#f3f3f3',
    marginTop:10,
    padding:10,
  },
  amountInput:{
    fontSize: 50,
    width:'65%',
    position:'absolute',
    right:0,
    top:'10%',
    textAlign: 'right', 
  },
  avoidView:{
  },
  pickerInputCard:{
    borderWidth: 1,
    borderColor: '#f3f3f3',
    height: 25,
    backgroundColor: '#fff',
    marginLeft:'-5%',
  },
  tagInput:{
    height:10,
  },
  btnSaveDisabled:{
    width:'100%',
    backgroundColor: '#dedede',
    textAlign: 'center',
    marginTop:10,
    padding:10, 
  },
  btnSaveValid:{
    width:'100%',
    backgroundColor: '#53b3bd',
    textAlign: 'center',
    color:'#fff',
    marginTop:10,
    padding:10, 
  },
  btnDelete:{
    width:'100%',
    backgroundColor: '#EBA0A1',
    color:'#ffffff',
    textAlign: 'center',
    marginTop:10,
    padding:10, 
  },
  currencyTransaction:{
    marginTop:20,
    padding:20,
    borderWidth: 1,
    width:'26%',
    backgroundColor: '#dedede'
  },
  moreCard:{
    width:'100%',
    marginTop:10,
    borderRadius: 15,
    flexDirection: 'row', 
    position: 'relative',
    backgroundColor: '#fff',
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
  },
  moreIcon:{
    width:'20%',
    marginTop:13,
  },
  moreLabel:{
    fontSize: 20,
    padding:15,
    width:'80%',
    fontWeight:'bold'
  },
  cancelButton:{
    backgroundColor: '#fff',
    borderRadius:15,
    marginBottom:20,
  },
  moreBody:{
    backgroundColor: '#f3f3f3',
    padding:20
  },
}

export {
  transactionForm,
}