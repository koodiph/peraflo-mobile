
import React from 'react';
import { ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";

// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign     from 'react-native-vector-icons/AntDesign';
import moment        from 'moment'
import Home          from './dashboard_screens/Home'
import LedgerEntry   from './dashboard_screens/LedgerEntry'
import { ModalPortal }          from 'react-native-modals';

const initialState = {
    view:'dashboard',
    transaction:{},
    wallets:[],
    filter:{},
};
export default class Dashboard extends React.Component {

  constructor(props){
    super(props);
    this.state = initialState;
  }


  componentDidMount() {

    if (global.user == null) {
      this.props.navigationProp('Login')
    }
    
    this.props.onRef(this);

  }

  navigationProp(route) {
    this.props.navigationProp(route)
  }

  componentWillUnmount() {

  }

  changeView(view){
    let state = this.state
    state.view = view
    this.setState(state)
  }

  changeHeader(header)
  {
    this.props.navigation.setOptions({headerTitle:header})
    this.props.navigation.setParams({title:header})
  }

  addWallets(wallets)
  {
    let state     = this.state
    state.wallets = wallets
    this.setState(state) 
  }
  
  selectTransaction(transaction){
    this.props.selectTransaction(transaction)
    // state.transaction = transaction
  }
  
  getTransaction(){
    let state = this.state
    return state.transaction
  }
  
  getWallets(){
    const state = this.state
    return state.wallets
  }
  
  async transactions() {
    await this.dashboard.reloadPage()
  }

  logout() {
    // this.props.navigationProp('Login')
  }

  render() {
    if (this.state.view == 'dashboard') {
      return (
        <Home  onRef={ref => (this.dashboard = ref)} navigationProp={this.navigationProp.bind(this)} changeView={this.changeView.bind(this)} addWallets={this.addWallets.bind(this)} selectTransaction={this.selectTransaction.bind(this)}  changeHeader={this.changeHeader.bind(this)}  logout={this.logout.bind(this)} />
      ); 
    }else if (this.state.view == 'entry') {
      return (
        <LedgerEntry getWallets={this.getWallets.bind(this)} selectTransaction={this.selectTransaction.bind(this)} getTransaction={this.getTransaction.bind(this)} changeView={this.changeView.bind(this)} logout={this.logout.bind(this)} />
      ); 
    }
  }
}



const styles = StyleSheet.create({
  body: {
    padding:10,
    marginBottom: 10,
  }
});
