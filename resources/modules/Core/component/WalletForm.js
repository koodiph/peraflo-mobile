import React from 'react';
import { View, Text,
         StyleSheet, TextInput,
         Picker, Button } from 'react-native';
import { BottomModal, ModalContent }  from 'react-native-modals';
import { common, wallet,
         transactionForm } from '../../helpers/styles'

import { formatPrice } from '../../helpers/filters'
import { FontAwesome5, Entypo,
         FontAwesome, Fontisto,
         MaterialIcons, Feather,
         MaterialCommunityIcons } from 'react-native-vector-icons';

import Toast from 'react-native-simple-toast';
import axios from 'axios'
import _ from 'lodash'

const initialState = {
  modalVisible:false,
  currencies:[],
  form:{
    owner           : '',
    currency        : null,
    amount          : '0.00',
    status          : 'active',
    type            : 'cash',
    label           : null,
    order           : 0,
    startingBalance : '0.00',
  },
  members:[],
  validForm:false,
  loader:false,
  selectedWallet:null,
}


export default class WalletForm extends React.Component {
  
  _isMounted = false;

  constructor(props){
    super(props);
    this.state = initialState;
  }

  async setModalVisible(clearForm = true) {

    if (clearForm) {
      await this.resetForm()
    }

    if (this.state.modalVisible == false) {
      await this.getWalletDetails()
    }

    let state = this.state
    state.modalVisible = !this.state.modalVisible
    this.setState(state)

  }

  selectWallet(wallet) {
    let state = this.state
    const form = {
      owner           : wallet.user_id,
      currency        : wallet.currency,
      amount          : `${formatPrice(wallet.balance)}`,
      status          : wallet.status,
      type            : wallet.type,
      label           : wallet.name,
      order           : wallet.order,
      startingBalance : wallet.starting_balance,
    }

    state.selectedWallet = wallet
    state.form = form
    this.setState(state)
    
    this.setModalVisible(false);
    this.validateForm()
  }

  async getWalletDetails() {
    const url = window.app_url + '/api/a/transaction-details'
    const user = global.user
    const auth_organization = user.auth_organization

    const data = {
      organization_id : user.auth_organization.organization_id,
      role            : user.role.name
    }

    try {
      const response   = await axios.post(url, data)
      let state        = this.state
      state.currencies = response.data.currencies
      state.members    = response.data.members

      if (state.currencies.length) {
        state.form.currency = state.currencies[0].code
      }
      this.setState(state)

    } catch(e) {
      console.log(e)
    }

  }

  inputValue(value, field) {
    let state = this.state
    let form  = this.state.form

    form[field] = value
    state.form  = form
    this.setState(state)

    this.validateForm()
  }

  componentDidMount() {
    this._isMounted = true;

    if (this._isMounted) {
      this.props.onRef(this);
    }
  }


  componentWillUnmount() {
    this.props.onRef(undefined);
    this._isMounted = false;
  }

  pickerCurrencyComponent() {
    return (

      <View
          style={{
            paddingVertical: 8,
            backgroundColor: '#fff',
            borderRadius: 20,
            overflow: 'hidden', 
            textAlign: 'center', 
        }}>
        <Picker
          style={{height: 24,backgroundColor: '#fff',marginLeft:'25%', }}
          selectedValue={this.state.form.currency}
          onValueChange={(itemValue, itemIndex) => this.inputValue(itemValue, 'currency')}
        >
        {this.state.currencies.map((currency, key) => {
          return <Picker.Item value={currency.code} key={key} label={currency.code}/>
        })}
      </Picker>
      </View>
    )
  }

  resetForm() {
    let state = this.state

    const form = {
      owner           : '',
      currency        : '',
      amount          : '0.00',
      status          : 'active',
      type            : 'cash',
      label           : null,
      order           : 0,
      startingBalance : '0.00',
    }

    state.form = form
    state.selectedWallet = null

    this.setState(state)
  }

  blurFormatPrice() {
    let form = this.state.form

    form.amount = formatPrice(form.amount.replace(/[^.\d]/g, ''))

    let state = this.state
    state.form = form
    this.setState(state)

    this.validateForm()
  }

  clearAmount() {
    const amount = this.state.form.amount
    let form = this.state.form
    let state = this.state

    if ('0.00' === amount) {
      form.amount = ''
    }
    state.amountFocus = true
    state.form = form

    this.setState(state)
  }

  blurStartingPrice() {

    let form = this.state.form

    form.startingBalance = formatPrice(form.startingBalance.replace(/[^.\d]/g, ''))

    let state = this.state
    state.form = form
    this.setState(state)

    this.validateForm()
  }
  
  clearStartingAmount() {
    const startingBalance = this.state.form.startingBalance
    
    let form = this.state.form
    let state = this.state

    if ('0.00' === startingBalance) {
      form.startingBalance = ''
    }
    state.form = form

    this.setState(state)
  }
  
  validateForm() {
    const form = this.state.form
        
    const requiredFields = [
      'currency',
      'label',
      'owner',
    ]

    let validation = true

    _.forOwn(requiredFields,function(field){
      if (validation && !form[field]) {
        validation = false
        console.log(validation, field)
      }
    })
    let state = this.state

    state.validForm = validation

    this.setState(state)
  }

  loader(value) {
    let state = this.state
    state.loader = value
    this.setState(state)
  }

  async save() {
    const state = this.state
    const form = state.form
    const user = global.user
    const auth_organization = user.auth_organization

    if (!state.validForm) {
      Toast.show('Please fill up required fields.');
      return
    }

    let url = global.app_url+`/api/a/wallet/store`

    const loader = state.loader
    form.organization_id = user.auth_organization.organization_id

    if (state.selectedWallet) {
      form.walletId = state.selectedWallet.id
      url = global.app_url+`/api/a/wallet/update`
    }

    if (!loader) {
      this.loader(true)
      try {
        const response = await axios.post(url, form)

        if (!response.data.error) {
          if (state.selectedWallet) {
            console.log('asl;dfasjkl;df')
          }
          this.resetForm()
          this.setModalVisible();
          Toast.show(response.data.message);
          await this.props.filterWallet()
        }else{
          if (response.data.error) {
            Toast.show(response.data.message);
          }else{
            Toast.show('Failed saving wallet.');
          }
        }

        this.loader(false)
      } catch(e) {
        console.log(e)
        Toast.show('Failed saving wallet.');
        this.loader(false)
      }
    }else{
      Toast.show('Currently processing another query.');
    }
  }

  async delete() {
    const _this = this
    const state = this.state
    const loader = state.loader
    const url = global.app_url+`/api/a/wallet/delete` 
    const user = global.user
    const auth_organization = user.auth_organization
    const data = {
      walletId:state.selectedTransaction.transaction_id,
      organizationId: user.auth_organization.organization_id,
    }
    Alert.alert(
      "Delete",
      "Are you sure?",
      [
        {
          text: "Yes",
          async onPress() {
            if (!loader) {
              _this.loader(true)
              try {
                const response = await axios.post(url, data)
                if (!response.data.error) {
                  _this.resetForm()
                  _this.setModalVisible();
                  _this.props.loadTransactions() 
                  Toast.show(response.data.message);
                }else{
                  if (response.data.error) {
                    Toast.show(response.data.message);
                  }else{
                    Toast.show('Failed deleting transaction.');
                  }
                }
                _this.loader(false)
              } catch(e) {
                Toast.show('Failed deleting transaction.');
                _this.loader(false)
              }
            }else{
              Toast.show('Currently processing another query.');
            }
          },
          style: "cancel"
        },
        { text: "Cancel", onPress() {
        }}
      ],
      { cancelable: false }
    );
  }

  startingBalanceComponent() {
    return (
      <View style={transactionStyle.rowCard}>
        <FontAwesome5 style={transactionStyle.rowIcon} name="money-bill-alt" size={20} />
        <Text style={transactionStyle.rowLabel}>Starting Balance</Text>
        <View style={transactionStyle.rowValue}>
          <TextInput  
            style={transactionStyle.textInput}  
            keyboardType={'numeric'}  
            defaultValue={this.state.form.startingBalance}
            onChangeText={(e) => this.inputValue(e, 'startingBalance')}
            onBlur={(e) => this.blurStartingPrice()}
            onFocus={() => this.clearStartingAmount()}
          />
        </View>
      </View>
    )
  }

  render() {
    return (
      <BottomModal
        visible={this.state.modalVisible}
      >
        <View>
          <View style={{backgroundColor: '#f3f3f3',padding:20}}>
            <View style={{width:'100%',flexDirection: 'row',position:'relative' }}>
              <View style={{width:'90%',}}></View>
              <MaterialCommunityIcons style={transactionStyle.cancelButton} name="close-circle" size={25} color={'#EBA0A1'} onPress={() => this.setModalVisible()}/>
            </View>
            <View style={transactionStyle.formRow}>
              <View style={transactionStyle.currency}>
                {this.pickerCurrencyComponent()}
              </View>
              {
                this.state.selectedWallet ? (
                    <Text style={transactionStyle.amountInput}>
                      {this.state.form.amount}
                    </Text>
                  ) : (
                    <TextInput  
                      style={transactionStyle.amountInput}  
                      keyboardType={'numeric'}  
                      defaultValue={this.state.form.amount}
                      onChangeText={(e) => this.inputValue(e, 'amount')}
                      onBlur={(e) => this.blurFormatPrice()}
                      onFocus={() => this.clearAmount()}
                    />
                  )
              }
            </View>
          </View>
          <View style={styles.modalBody}>
            {this.state.selectedWallet ? this.startingBalanceComponent() : null}
            <View style={transactionStyle.rowCard}>
              <FontAwesome style={transactionStyle.rowIcon} name="sticky-note-o" size={20} />
              <Text style={transactionStyle.rowLabel}>Name</Text>
              <View style={transactionStyle.rowValue}>
                <TextInput  
                  style={transactionStyle.textInput}
                  defaultValue={this.state.form.label}
                  onChangeText={(inputText) => this.inputValue(inputText, 'label')}
                />
              </View>
            </View>
            {
              this.state.selectedWallet ? null : (
                <View style={transactionStyle.rowCard}>
                  <Entypo style={transactionStyle.rowIcon} name="credit-card" size={20} />
                  <Text style={transactionStyle.rowLabel}>Type</Text>
                  <View style={transactionStyle.rowValue}>
                    <Picker
                      style={transactionStyle.pickerInputCard}
                      selectedValue={this.state.form.type}
                      onValueChange={(itemValue, itemIndex) => this.inputValue(itemValue, 'type')}
                    >
                      <Picker.Item value={'cash'} key={0} label={'Cash'}/>
                      <Picker.Item value={'credit card'} key={1} label={'Credit Card'}/>
                    </Picker>
                  </View>
                  <Entypo style={transactionStyle.rowIndicator} name="chevron-right" size={20} />
                </View>
              )
            }
            <View style={transactionStyle.rowCard}>
              <Entypo style={transactionStyle.rowIcon} name="classic-computer" size={20} />
              <Text style={transactionStyle.rowLabel}>Status</Text>
              <View style={transactionStyle.rowValue}>
                <Picker
                  style={transactionStyle.pickerInputCard}
                  selectedValue={this.state.form.status}
                  onValueChange={(itemValue, itemIndex) => this.inputValue(itemValue, 'status')}
                >
                  <Picker.Item value={'active'} key={0} label={'Active'}/>
                  <Picker.Item value={'inactive'} key={1} label={'Inactive'}/>
                </Picker>
              </View>
              <Entypo style={transactionStyle.rowIndicator} name="chevron-right" size={20} />
            </View>
            {
              this.state.selectedWallet ? null : (
                <View style={transactionStyle.rowCard}>
                  <Fontisto style={transactionStyle.rowIcon} name="person" size={20} />
                  <Text style={transactionStyle.rowLabel}>Owner</Text>
                  <View style={transactionStyle.rowValue}>
                    <Picker
                      style={transactionStyle.pickerInputCard}
                      selectedValue={this.state.form.owner}
                      onValueChange={(itemValue, itemIndex) => this.inputValue(itemValue, 'owner')}
                    >
                      <Picker.Item value={null} key={0} label={'Select Owner'}/>
                      {this.state.members.map(function(member, key){
                        return (<Picker.Item value={member.id} key={key} label={`${member.first_name} ${member.last_name}`}/>)
                      })}
                    </Picker>
                  </View>
                </View>
              )  
            }

            <View style={{marginTop:20,}}>
              <Button title="SAVE" style={this.state.validForm ? transactionStyle.btnSaveValid : transactionStyle.btnSaveDisabled} onPress={() => this.save()} color={this.state.validForm ? '#53b3bd': '#dedede'} />
              {this.state.selectedWallet ? (
                // <Text onPress={() => this.delete()} style={styles.btnDelete}>
                //   DELETE
                // </Text>
                null
                ) : null}
            </View>

          </View>
        </View>
      </BottomModal>
    )
  }
}

const commonStyle = StyleSheet.create(common);
const styles = StyleSheet.create(wallet)
const transactionStyle = StyleSheet.create(transactionForm)