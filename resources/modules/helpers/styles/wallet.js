const wallet = {
  screenHeader:{
    marginTop:0,
    borderBottomWidth: 1,
    borderBottomColor: '#dedede',
    paddingTop:10,
    paddingBottom: 10,
    backgroundColor: '#fff',
    paddingLeft: 10,
    position: 'relative', 
    flexDirection: 'row', 
  },
  textLabel:{
    fontSize: 25,
    width:'89%',
  },
  walletButton:{
    width:'10%',
    paddingTop:'0.5%',
    flexDirection: 'row', 
  },
  headerButton:{
    borderWidth:1,
    borderColor: '#dedede',
    fontSize: 10,
    padding:10,
    marginRight: 10,
    marginLeft: 10,
    textAlign: 'center', 
    fontWeight: 'bold', 
  },
  walletListCard:{
    borderWidth: 1,
    borderColor: '#dedede',
    padding:10,
    position:'relative', 
    backgroundColor: '#fff',
    marginTop:10,
    marginBottom:10,
    borderRadius:15,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 1,
  },
  walletListAmount: {
    width:'50%',
    paddingTop:'1%',
    flexDirection: 'row-reverse',
  },
  walletListCurrencyLabel:{
    fontSize: 12,
    fontWeight: 'bold', 
    marginTop:'2%',
  },
  walletListAmountLabel:{
    textAlign: 'right', 
    fontWeight: 'bold', 
    // backgroundColor: 'black',
    fontSize: 25,
    position:'relative',

  },
  walletContainer:{
    height: '91%',
    borderBottomWidth: 1,
    borderBottomColor: '#dedede',
  },
  walletLabel:{
    marginBottom: 5,
    fontWeight: 'bold', 
    width:'95%',
  },
  bottomFilter:{
    backgroundColor: '#dedede',
    paddingTop:20,
  },
  filterRow:{
    flexDirection: 'row',
    width:"100%",
    borderBottomWidth: 1,
    paddingLeft:10,
    paddingBottom:15,
    marginBottom:15,
    position:'relative',
  },
  filterIcon:{
    position:'absolute',
    left:10,
    top:-3,
    fontWeight: 'bold',
  },
  filterLabel:{
    marginLeft:30,
    width:'85%'
  },
  modalBody:{
    backgroundColor: '#f3f3f3',
    padding:20,
  },
  modalWalletLabel: {
    fontSize: 15, 
    fontWeight: 'bold'
  },
  cardText:{
    fontSize: 20,
    fontWeight: 'bold', 
  },
  btnTransfer:{
    width:'50%',
    textAlign: 'center',
    backgroundColor: '#53b3bd',
    color:'#fff',
    height: '65%',
    paddingTop:1,
    paddingBottom: 1
  },
  headerCard:{
    width:'45%',
    // backgroundColor: '#53b3bd50',
    backgroundColor: '#fff',
    padding:10,
    margin:10,
    borderRadius:15,
  },
  headerSection:{
    paddingTop:0,
    flexDirection: 'row', 
    // backgroundColor: '#1cd88d',
    backgroundColor: '#53b3bd',
  },
}


export {
  wallet,
}