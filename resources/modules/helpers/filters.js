import moment from 'moment'

function formatPrice(value) {
  if (value) {
    let val = (value/1).toFixed(2).replace(',', '.')
    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }
  return '0.00'
}

function capitalize (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
}

function formatNumber (value) {
  if (!value) return ''
  return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
}

function formatDateYMD (value) {
  if (value) {
    return moment(String(value)).format('MMMM DD, Y')
  }
}

function formatDateMDY (value) {
  if (value) {
    return moment(String(value)).format('MMMM DD, Y')
  }
}

function formatDate (value) {
  if (value) {
    return moment(String(value)).format('DD')
  }
}

function formatGetDayMedium (value) {
  if (value) {
    return moment(String(value)).format('MM.YYYY ddd')
  } 
}

function formatGetDay (value) {
  if (value) {
    return moment(String(value)).format('ddd')
  } 
}

function formatDateDay (value) {
  if (value) {
    return moment(String(value)).format('dddd, MMMM DD')
  }
}

function specialFormat (value) {
  if (value) {
    return moment(String(value)).format('MMM DD Y (dddd)')
  }
}

function dateFormat (value) {
  if (value) {
    return moment(String(value)).format('MMM DD Y')
  }
}

function monthYear (value) {
  if (value) {
    return moment(String(value)).format('MMM Y')
  }
}

function timeFormat (value) {
  if (value) {
    return moment(String(value)).format('hh:mm A')
  }
}

function filterYear(value) {
  if (value) {
    return moment(String(value)).format('Y')
  }
}

function filterMonth(value) {
  if (value) {
    return moment(String(value)).format('MMM')
  }
}

function formatDateShort (value) {
  if (value) {
    return moment(String(value)).format('MMM-DD')
  }
}

function formatDateDashed (value) {
  if (value) {
    return moment(String(value)).format('MMM-DD-YY')
  }
}
function formatDay(value) {
  if (value) {
    return moment(String(value)).format('DD')
  }
}

function formatDateYMDwithTime(value) {
  if (value) {
    return moment(String(value)).format('MMMM DD, Y hh:mm A')
  }
}

function dateToYM (date) {
    var m = date.getMonth() + 1;
    var y = date.getFullYear();
    return '' + y + '-' + (m<=9 ? '0' + m : m);
}
  
function dateToYMD (date) {
  var d = date.getDate();
  var m = date.getMonth() + 1; //Month from 0 to 11
  var y = date.getFullYear();
  return '' + y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
}


function emptyObject(filtered) {
  return Object.entries(filtered).length === 0
}

function previewText(text) {
  if (text && text.length > 25) {
    return `${text.substr(0,25)}..`
  }
  return text
}

function cleanDecimal(value) {
  return parseFloat(value.replace(/,/g, '')).toFixed(2)
}


export {
  formatPrice,
  capitalize,
  formatNumber,
  formatDateYMD,
  formatDate,
  formatDay,
  formatGetDayMedium,
  formatGetDay,
  formatDateDay,
  specialFormat,
  dateFormat,
  monthYear,
  timeFormat,
  filterYear,
  filterMonth,
  formatDateShort,
  formatDateDashed,
  formatDateMDY,
  formatDateYMDwithTime,
  dateToYM,
  dateToYMD,
  emptyObject,
  previewText,
  cleanDecimal,
}
