import React, { useState } from 'react';
import { KeyboardAvoidingView, Platform,
         View, Button,
         Text, Picker,
         StyleSheet, TextInput,
         TouchableOpacity, SafeAreaView,
         Keyboard, Alert } from 'react-native';
import { Modal, ModalContent,
         BottomModal, ModalFooter,
         ModalButton }  from 'react-native-modals';
import { wallet, common,
         transactionForm } from '../../helpers/styles'
import { FontAwesome5, Entypo,
         FontAwesome, Fontisto,
         MaterialIcons, MaterialCommunityIcons,
         AntDesign } from 'react-native-vector-icons';


const initialState = {
  modalVisible:false,
  currencies:[],
  filter:{
    currency:'',
    status:'active',
  },
}

export default class WalletFilter extends React.Component {

  _isMounted = false;

  constructor(props){
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    
    this._isMounted = true;

    if (this._isMounted) {
      this.props.onRef(this);
    }
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
    this._isMounted = false;

  }

  setFilter(value, field) {
    let state = this.state
    let filter = this.state.filter
    filter[field] = value
    state.filter = filter
    this.setState(state)
  }

  setModalVisible(data = {}) {
    let state = this.state
    let filter = this.state.filter

    state.modalVisible = !this.state.modalVisible
    
    if (state.modalVisible) {
      state.currencies = data.currencies
    }

    if (state.modalVisible && state.currencies.length) {
      state.filter.currency = state.currencies[0].code
    }

    if (state.modalVisible && filter) {
      state.filter.currency = filter.currency
    }

    this.setState(state)

  }

  async applyFilter() {
    await this.props.filterWallet()
    this.setModalVisible()
  }

  render() {
    return (
      <BottomModal
        visible={this.state.modalVisible}
        onTouchOutside={() => {
          this.setModalVisible();
        }}
        onSwipeOut={() => this.setModalVisible()}
      >
      <View style={{backgroundColor: '#f3f3f3'}}>
        <View style={formStyle.cardBody}>
          <View style={formStyle.rowMore}>
            <Text style={{width:'100%'}} >Filters</Text>
          </View>
          <View style={formStyle.rowCard}>
            <FontAwesome5 style={formStyle.rowIcon} name="dollar-sign" size={20} />
            <Text style={formStyle.rowLabel}>Currency</Text>
            <View style={formStyle.rowValue}>
              <Picker
                  style={formStyle.pickerInputCard}
                selectedValue={this.state.filter.currency}
                onValueChange={(itemValue, itemIndex) => this.setFilter(itemValue, 'currency')}
              >
              {this.state.currencies.map((currency, key) => {
                return <Picker.Item value={currency.code} key={key} label={currency.code}/>
              })}
              </Picker>
            </View>
          </View>
          <View style={formStyle.rowCard}>
            <Fontisto style={formStyle.rowIcon} name="checkbox-active" size={20} />
            <Text style={formStyle.rowLabel}>Status</Text>
            <View style={formStyle.rowValue}>
              <Picker
                style={formStyle.pickerInputCard}
                selectedValue={this.state.filter.status}
                onValueChange={(itemValue, itemIndex) => this.setFilter(itemValue, 'status')}
              >
                <Picker.Item value={'active'} key={0} label={'Active'}/>
                <Picker.Item value={'inactive'} key={1} label={'Inactive'}/>
              </Picker>
            </View>
          </View>

          <View style={{width:'100%',marginTop:20}}>
            <Button title="Apply Filter" style={{width:'100%'}} onPress={() => this.applyFilter()} color={'#53b3bd'} />
          </View>
        </View>
        </View>
      </BottomModal>
    )
  }
}

const commonStyle = StyleSheet.create(common);
const styles = StyleSheet.create(wallet)
const formStyle = StyleSheet.create(transactionForm)
