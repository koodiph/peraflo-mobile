import React, { useState } from 'react';
import { KeyboardAvoidingView, Platform,
         View, Button,
         Text, Picker,
         StyleSheet, TextInput,
         TouchableOpacity, SafeAreaView,
         Keyboard, Alert, 
         TouchableNativeFeedback } from 'react-native';
import { Modal, ModalContent,
         BottomModal, ModalFooter,
         ModalButton }  from 'react-native-modals';
import { organization } from '../../helpers/styles'
import { MaterialIcons, AntDesign,
         FontAwesome, MaterialCommunityIcons,
         FontAwesome5 } from 'react-native-vector-icons';
import axios from 'axios'

const initialState = {
  modalVisible:false,
  organizations:[],
  authOrganization:null,
}

export default class OrganizationModalSelect extends React.Component {

  _isMounted = false;

  constructor(props){
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    
    this._isMounted = true;

    if (this._isMounted) {
      this.props.onRef(this);
    }
  }

  componentWillUnmount() {
    this.props.onRef(undefined);
    this._isMounted = false;

  }

  setModalVisible() {
    let state = this.state

    state.modalVisible = !this.state.modalVisible
    
    this.setState(state)
  }

  async openModal() {
    await this.getOrganizations()
    await this.setModalVisible()
  }

  selectAuth(organization) {
    let state = this.state
    state.authOrganization = organization
    this.setState(state)
  }

  async getOrganizations() {
    const url = window.app_url+'/api/a/organizations'
    const user = global.user
    this.selectAuth(user.auth_organization)

    let state = this.state
    state.organizations = user.organizations
    this.setState(state)

    // const data = user.auth_organization    
    // try {
    //   const response = await axios.post(url, data)
    //   let state = this.state
    //   state.organizations = response.data.organizations
    //   this.setState(state)
    // } catch(e) {
    //   console.log(e)
    // }

  }

  async selectItem(item){
    const data ={
      label : item.name,
      value : item.organization_id,
      data  : item,
    }
    this.setModalVisible()

    this.selectAuth(item)
    this.props.selectItem(data)
    this.changeOrganization(item)
  }

  async changeOrganization(data) {
    const url = global.app_url+'/api/a/change/organizations'
    try {
      const response = await axios.post(url, data)
    } catch(e) {
    }
  }

  organizationList(organization, key) {
    const component = 
      <TouchableNativeFeedback
        onPress={() => this.selectItem(organization)} 
        background={TouchableNativeFeedback.Ripple('#dedede', false)}
        key={key} >
      <View style={this.state.authOrganization.organization_id === organization.organization_id ? styles.selected : styles.selector}>
        <Text style={styles.organizationName}>{organization.name}</Text>
        {this.state.authOrganization.organization_id === organization.organization_id ? <FontAwesome5 name={'check-circle'} color='green' size={25} style={{fontWeight:'bold',width:'90%'}} /> : null}
      </View>
      </TouchableNativeFeedback>
    return (component)
  }


  render() {
    const _this = this
    return (
      <BottomModal
        visible={this.state.modalVisible}
        onTouchOutside={() => {
          this.setModalVisible();
        }}
        onSwipeOut={() => this.setModalVisible()}
      >
        <View style={styles.body}>
          {this.state.organizations.map((organization, key) => {
            return _this.organizationList(organization, key)
          })}
        </View>
      </BottomModal>
    )
  }
}


const styles = StyleSheet.create(organization)