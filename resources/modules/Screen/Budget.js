
import React from 'react';
import { ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";

// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign     from 'react-native-vector-icons/AntDesign';
import FontAwesome   from 'react-native-vector-icons/FontAwesome';
import moment        from 'moment'

import Home          from './wallet_screens/Home'
import Activities    from './wallet_screens/Activities'
import Forms         from './wallet_screens/Forms'

const initialState = {
    view:'home',
    active_wallet:null,
};

export default class Budget extends React.Component {

  constructor(props){
    super(props);
    this.state = initialState;
    this.changeView.bind(this)
    if (!global.user) {
      this.props.navigationProp('Login')
    }
  }

  mainNavitionProp(header){
    // const { navigate } = this.props.navigation;
    this.props.mainNavitionProp(header)
    // this.props.navigation.goBack()
    // navigate('Login',);
  }

  componentDidMount() {
    var _this = this
    var state = initialState
    this.setState(state)
  }

  componentWillUnmount() {
    var _this = this
  }

  changeView(view){
    var _this = this
    var array_view = view.split('-')
    var state = _this.state

    if (array_view[0] == 'activities' || array_view[0] == 'forms') {
        state.view          = array_view[0];
        state.active_wallet = array_view[1];
    }else{
        state.view = array_view[0];
        state.active_wallet = null;
    }
    _this.setState(state)
  }

  goToPage(url){
    Linking.canOpenURL(global.app_url + url).then(supported => {
      if (supported) {
        Linking.openURL(global.app_url + url);
      } else {
        console.log("Don't know how to open URI: " + global.app_url + url);
      }
    });
  }

  activeWallet(){
    return this.state.active_wallet
  }
  
  navigationProp(){
    var data  = global.user
    var _this = this
    axios.post(global.app_url+'/api/a/logout', data).then(function(response){
      global.user = null
      _this.props.navigationProp('Login')
    }).catch(function(error){
      console.log(error)
    });
  }

  render() {
    // /setting/security
        this.mainNavitionProp('Settings')
        return (
          <View style={styles.body}>
            <View style={styles.setting_header}>
                <Text style={styles.s_header}>Manage</Text>
            </View>
            <View style={styles.setting_list}>
                <View style={styles.sl_list}>
                    <MaterialIcons name="account-balance" color='black' size={25} onPress={() => this.goToPage('/bill')} />
                    <Text style={styles.sl_text} onPress={() => this.goToPage('/bill')}>
                        Bills
                    </Text>
                </View>
                <View style={styles.sl_list}>
                    <MaterialIcons name="account-balance-wallet" color='black' size={25} onPress={() => this.goToPage('/budget')} />
                    <Text style={styles.sl_text} onPress={() => this.goToPage('/budget')}>Budgets</Text>
                </View>
                <View style={styles.sl_list}>
                    <MaterialIcons name="assignment-ind" color='black' size={25} onPress={() => this.goToPage('/loans')} />
                    <Text style={styles.sl_text} onPress={() => this.goToPage('/loans')}>Loans</Text>
                </View>
            </View>
          </View>
        ); 

  }
}


const styles = StyleSheet.create({
  body: {
    padding:10,
    marginBottom: 10,
  },
  setting_header:{
    width:'100%',
    backgroundColor: '#53b3bd',
    padding:10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  s_header:{
    color:'white',
  },
  setting_list:{
    width:'100%',
    borderColor: '#53b3bd',
    borderWidth: 1,
  },
  sl_list:{
    borderTopColor: '#dedede',
    borderTopWidth: 1,
    padding:5,
    flexDirection: 'row', 
  },
  sl_text:{
    fontSize: 20,
    marginLeft:10,
  },
});
