
import React from 'react';
import { ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";

// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign     from 'react-native-vector-icons/AntDesign';
import moment        from 'moment'

const initialState = {
  wallet_id:null,
  dateFilter:null,
  wallets_logs:[],
};

export default class Activities extends React.Component {

  constructor(props){
    super(props);
    initialState.wallet_id = this.activeWallet()
    initialState.date_filter = this.dateToYMD(new Date())
    if (!initialState.wallet_id) {
      this.changeView('home')
    }
    this.state = initialState;
  }


  componentDidMount() {
    var _this = this
    _this.getWalletLogs()
  }

  activeWalletModel()
  {
    return this.props.activeWalletModel()
  }

  reloadPage()
  {
    var _this = this
    this.changeView('home')
  }

  getWalletLogs(){
    var _this = this

    var user  = global.user;
    var data = {
      date_filter:this.state.date_filter,
      wallet_id:this.state.wallet_id,
      organization_id:user.auth_organization.id,
    }
    axios.post(global.app_url+'/api/a/wallet/logs', data).then(function(response){
      var state = _this.state
      state.wallets_logs = response.data
      _this.setState(state)
    }).catch(function(error){
      console.log(error)
    });
  }
  // filter date start
  monthProceed(action){
      var _this = this
      if (action == 'next') {
          var date =  moment(String(this.state.date_filter)).add(1, 'M').format('MMMM DD, Y')
      }else if (action == 'prev') {
          var date =  moment(String(this.state.date_filter)).add(-1, 'M').format('MMMM DD, Y')
      }
      var state = _this.state
      state.date_filter = _this.dateToYMD(new Date(date))
      _this.setState(state)
      _this.getWalletLogs()
  }

  dateToYMD (date) {
      var d = date.getDate();
      var m = date.getMonth() + 1; //Month from 0 to 11
      var y = date.getFullYear();
      return '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
  }

  monthYear(value) {
      return moment(String(value)).format('MMM Y')
  }
  // filter date end

  activeWallet()
  {
    return this.props.activeWallet()
  }


  changeView(view){
    var _this = this
    this.props.changeView(view);
  }

  monthYear(value) {
      return moment(String(value)).format('MMM Y')
  }



  formatPrice(value) {
    if (value) {
        let val = (value/1).toFixed(2).replace(',', '.')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }else{
        return '0.00'
    }
  }

  render() {
    var table_header = [
      'Date',        
      'User',        
      'Beginning Balance',        
      'Transaction',        
      'Current Balance',        
    ];
    var table_data = [
      // [
      //   'June 3, 2020 01:52am',        
      //   'Admin admin',        
      //   'PHP 15,000.00',        
      //   '+ PHP 2.00',        
      //   'PHP 15,002.00',        
      // ],
    ];
      this.state.wallets_logs.map((wallet, key) =>{
        var new_data = [];
        if (wallet.a_transaction) {
          new_data.push(wallet.date)
          if (wallet.user) {
            new_data.push(wallet.user.first_name+' '+wallet.user.last_name)
          }else{
            new_data.push(null)
          }
          new_data.push(wallet.beginning_balance)
          new_data.push(wallet.a_transaction)
          new_data.push(wallet.current_balance)
          table_data.push(new_data)
        }
      })
    var wallet_model = this.activeWalletModel()
    return (
      <View style={styles.body}>
        <View style={styles.row}>
            <View style={styles.half_header_left}>
                <AntDesign style={{marginRight:10}} name="back" color='black' size={15}  onPress={() => this.changeView('home')} />
                <Text  onPress={() => this.changeView('home')} >Back</Text>
            </View>
            <View style={styles.half_header_right}>
                <Text  onPress={() => this.changeView('forms_'+this.state.wallet_id)} >Forms</Text>
                <AntDesign style={{marginLeft:10}} name="form" color='black' size={15}  onPress={() => this.changeView('home')} />
            </View>
        </View>
        <View style={styles.card}>
          <Text style={styles.c_wallet_name}>{wallet_model.name}</Text>
          <View style={styles.c_row}>
            {wallet_model.user ? (<Text style={styles.c_wallet_user}>{wallet_model.user.first_name} {wallet_model.user.last_name}</Text>) : null}
            <Text style={styles.c_currency}>{wallet_model.currency}</Text>
            <Text style={styles.c_amount}>{this.formatPrice(wallet_model.current_amount)}</Text>
          </View>
        </View>
        <View style={styles.dateFilter}>
          <View style={styles.filterDateIcon}>
              <AntDesign name="doubleleft" color='black' size={15}  onPress={() => this.monthProceed('prev')} />
          </View>
          <Text style={styles.dateFilterComponents}> {this.monthYear(this.state.date_filter)} </Text>
          <View 
          style={styles.filterDateIcon}>
              <AntDesign name="doubleright" color='black' size={15} onPress={() => this.monthProceed('next')} />
          </View>
        </View>
        <ScrollView style={styles.table_scroll}>
          <Table style={styles.table}>
            <Row data={table_header} style={styles.t_head} textStyle={styles.t_h_text}/>
            <Rows data={table_data} textStyle={styles.t_text}/>
          </Table>
        </ScrollView>
      </View>
    );
  }

}


const styles = StyleSheet.create({
  body: {
    padding:10,
    marginBottom: 10,
  },
  table_scroll:{
    marginTop:10,
    height:'65%',
    width:'100%',
  },
  half_header_left:{
    flexDirection: 'row', 
    width:'50%'
  },
  half_header_right:{
    paddingLeft: '30%',
    flexDirection: 'row', 
    width:'50%',
  },
  card:{
    width:'100%',
    paddingTop:10,
    paddingBottom:10,
    backgroundColor: '#53b3bd',
    paddingLeft:20,
    marginBottom: 10,
  },
  c_wallet_name:{
    color:'white',
    fontSize: 35,
    fontWeight: 'bold', 
  },
  c_row:{
    flexDirection: 'row', 
    marginTop:10,
  },
  c_wallet_user:{
    color:'white',
    fontSize: 25,
    width:'45%',
  },
  c_currency:{
    color:'white',
    fontSize: 15,
    width:'10%',
  },
  c_amount:{
    color:'white',
    fontSize: 25,
    width:'45%',
  },
  dateFilter:{
    flexDirection: 'row', 
  },
  dateFilterComponents:{
    fontSize: 20
  },
  filterDateIcon:{
    margin:5
  },
  row:{
    marginTop:10,
    marginBottom:10,
    flexDirection: 'row', 
  },
  table:{
    marginTop:10,
  },
  t_head:{
    backgroundColor: '#dedede',
    padding:5,
  },
  t_h_text:{
    fontSize:15,
  },
  t_text:{
    fontSize:15,
    padding:5,
  },
  t_row:{
    borderColor: '#dedede',
    borderWidth: 3,
  }
});
