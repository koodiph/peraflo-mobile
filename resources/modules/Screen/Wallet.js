
import React from 'react';
import { ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";

// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign     from 'react-native-vector-icons/AntDesign';
import moment        from 'moment'

import Home          from './wallet_screens/Home'
import AuditLog      from './wallet_screens/AuditLog'

const initialState = {
    view:'home',
    wallet_model:{},
    wallets:[],
    activeWallet:{},
};

export default class Dashboard extends React.Component {

  _isMounted = false;

  constructor(props){
    super(props);
    this.state = initialState;
    if (!global.user) {
      this.props.navigationProp('Login')
    }
  }

  componentDidMount() {
    this.props.onRef(this);
  }


  componentWillUnmount() {
    this.props.onRef(undefined);
    this._isMounted = false;
  }

  async changeView(view, wallet = {}) {
    let state = this.state

    state.view         = view
    state.activeWallet = wallet
    
    await this.setState(state)
  }

  setWalletModel(wallet) {
    var _this          = this
    var state          = _this.state
    state.wallet_model = wallet
    _this.setState(state)
  }

  setWallets(wallets) {
    var _this     = this
    var state     = _this.state
    state.wallets = wallets
    _this.setState(state)
  }

  activeWallet() {
    return this.state.activeWallet
  }

  activeWalletModel() {
    return this.state.wallet_model
  }

  activeWallets() {
    return this.state.wallets
  }

  openFilter(data) {
    this.props.openWalletFilter(data)
  }

  applyFilter(filter) {
    if ('home' === this.state.view) {
      this.homeScreen.applyFilter(filter)
    }else if ('audit-log' === this.state.view) {
      this.auditLogScreen.getWallet()
    }
  }

  selectWallet(wallet) {
    this.props.selectWallet(wallet)
  }

  getParent() {
    return this
  }

  openModal() {
    this.props.openModal()
  }

  async reloadPage() {

    if ('home' === this.state.view) {
      this.homeScreen.reloadPage()
    } else {
      await this.changeView('home')
      this.homeScreen.reloadPage()
    }
    global.reload_page = this.homeScreen
  }

  transferWallet(wallet) {
    this.props.transferWallet(wallet)
  }

  async reloadWallet() {
    if ('home' === this.state.view) {
      this.homeScreen.getWallets()
    }else if ('audit-log' === this.state.view) {
      await this.auditLogScreen.reloadPage()
      await this.auditLogScreen.getWallet()
    }
  }

  render() {
    if ('home' === this.state.view) {
      return (
        <Home onRef={ref => (this.homeScreen = ref)} getParent={this.getParent.bind(this)} transferWallet={this.transferWallet.bind(this)}  openModal={this.openModal.bind(this)} setWallets={this.setWallets.bind(this)} selectWallet={this.selectWallet.bind(this)} openFilter={this.openFilter.bind(this)} changeView={this.changeView.bind(this)} setWalletModel={this.setWalletModel.bind(this)} />
      )
    } else if ('audit-log' === this.state.view) {
      return (
        <AuditLog onRef={ref => (this.auditLogScreen = ref)} getParent={this.getParent.bind(this)} activeWallet={this.activeWallet.bind(this)} selectWallet={this.selectWallet.bind(this)} transferWallet={this.transferWallet.bind(this)}  changeView={this.changeView.bind(this)}/>
      )
    }
  }
}
