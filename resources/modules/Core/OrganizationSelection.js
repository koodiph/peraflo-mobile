
import React from 'react';
import { ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid, AsyncStorage } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";

// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign     from 'react-native-vector-icons/AntDesign';
import FontAwesome   from 'react-native-vector-icons/FontAwesome';
import moment        from 'moment'

const initialState = {
    view:'home',
    active_wallet:null,
    organizations:[],
};

export default class OrganizationSelection extends React.Component {

  constructor(props){
    super(props);
    initialState.organizations = global.user.organizations
    this.state                 = initialState;

  }

  componentDidMount() {
    var _this = this
    var state = initialState
    this.setState(state)
  }


  selectItem(item){
    var data ={
      label:item.name,
      value:item.id,
      data:item,
    }
    global.org_select.selectItem(data)
  }

  render() {
    var _this = this
    var org_id = global.user.auth_organization.id
    var organization_list = _this.state.organizations.map((organization, key) => {
      var icon = 'account-circle'
      if (organization.id == org_id) {
        icon = 'check-circle'
      }
       return (<View key={key} style={styles.sl_list}>
                  <MaterialIcons name={icon} color='black' size={25} onPress={() => this.selectItem(organization)} />
                  <Text style={styles.sl_text} onPress={() => this.selectItem(organization)}>
                      {organization.name}
                  </Text>
              </View>)
     })
    return (
        <View style={styles.body}>
          <View style={styles.setting_header}>
              <Text style={styles.s_header}>Organizations</Text>
          </View>
          <View style={styles.setting_list}>
          {organization_list}
          </View>
        </View>
    ); 
  }
}


const styles = StyleSheet.create({
  body: {
    padding:10,
    marginBottom: 10,
  },
  setting_header:{
    width:'100%',
    backgroundColor: '#53b3bd',
    padding:10,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  s_header:{
    color:'white',
  },
  setting_list:{
    width:'100%',
    borderColor: '#53b3bd',
    borderWidth: 1,
  },
  sl_list:{
    borderTopColor: '#dedede',
    borderTopWidth: 1,
    padding:5,
    flexDirection: 'row', 
  },
  sl_text:{
    fontSize: 20,
    marginLeft:10,
  },
});
