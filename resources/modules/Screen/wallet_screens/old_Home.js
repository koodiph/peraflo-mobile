
import React from 'react';
import { ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";

// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign     from 'react-native-vector-icons/AntDesign';
import moment        from 'moment'


// status: "active"
// wallet: "my_wallet"


const initialState = {
    wallets:[],
    currency:'',
    total_amount:'',
};

export default class Home extends React.Component {

  constructor(props){
    super(props);
    this.state         = initialState;
    global.view        = 'wallet_home'
    global.reload_page = this
    this.reloadPage()
  }


  reloadPage()
  {
    this.getWallets()
  }

  getWallets(){
    var _this = this
    var user  = global.user;
    var data = {
      organization_id:user.auth_organization.id,
      status:'active',
      wallet:'my_wallet',
      role:user.auth_organization.role,
    }
    // axios.post(global.app_url+'/api/a/wallets', data).then(function(response){
    //   var state          = _this.state
    //   state.wallets      = response.data.wallets
    //   state.total_amount = response.data.total_amount
    //   if (response.data.total_amount) {
    //     var array_amount = response.data.total_amount.split(' ')
    //     if (array_amount.length == 2) {
    //       state.currency     = array_amount[0]
    //       state.total_amount = array_amount[1] 
    //     }
    //   }
    //   _this.props.setWallets(state.wallets);
    //   _this.setState(state)
    // }).catch(function(error){
    //   console.log(error.response.data)
    // });
  }

  changeView(view, wallet = {}){
    var _this = this
    this.props.setWalletModel(wallet);
    this.props.changeView(view);
  }
  

  formatPrice(value) {
    if (value) {
        let val = (value/1).toFixed(2).replace(',', '.')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }else{
        return '0.00'
    }
  }


  render() {
    var _this = this
    var table_header = [
      'Name',        
      'Amount',        
      'Action',        
    ];

    var no_row = [
      'no wallet',        
    ];
    var table_datas = []
    _this.state.wallets.map((wallet,key) => {
      var onepush = [];
      onepush.push(wallet.name)
      onepush.push(wallet.currency+' '+_this.formatPrice(wallet.current_amount))
      onepush.push((<Text style={styles.t_action_text} onPress={() => this.changeView('activities_'+wallet.id, wallet)}>ACTIVITIES</Text>))
      table_datas.push(onepush)
    })
    
    return (
      <View style={styles.body}>
        <View style={styles.row}>
          <View style={styles.modal_button}>
          </View>
        </View>
        <View style={styles.row}>
          <Text style={styles.total_wallet_label}>Total Wallet Value</Text>
        </View>
        <View style={styles.row}>
          <Text style={styles.total_wallet_currency}>{this.state.currency}</Text>
          <Text style={styles.total_wallet_amount}>{this.state.total_amount}</Text>
        </View>
        <ScrollView style={styles.table_scroll}>
          <Table style={styles.table}>
            <Row data={table_header} style={styles.t_head} textStyle={styles.t_h_text}/>
            <Rows data={table_datas} style={styles.t_row}  textStyle={styles.t_text}/>
          </Table>
        </ScrollView>
        <View style={styles.add_icon}>
          <View style={styles.icon_bg}></View>
          <MaterialIcons name="add-circle" onPress={() => this.changeView('wallet-forms')} color='#36858d' size={50} />
        </View>
      </View>
    );
  }

}


const styles = StyleSheet.create({
  body: {
    padding:10,
    marginBottom: 10,
  },
  table_scroll:{
    marginTop:10,
    height:375,
    width:'100%',
  },
  row:{
    marginTop: 10,
    flexDirection: 'row', 
  },
  header:{
    fontSize: 35,
    width:'75%',
  },
  modal_button:{
    marginLeft:15,
    marginTop: 10
  },
  total_wallet_amount:{
    marginLeft:10,
    fontSize:30,
  },
  table:{
    marginTop: 10,
    width:'100%'
  },
  t_head:{
      backgroundColor:'#343a40',
      borderColor:'#454d55',
      borderWidth:2,
  },
  t_h_text:{
    textAlign: 'center', 
    color:'white',
    padding:10,
  },
  t_text:{
    padding:10,
  },
  t_action_text:{
    padding:10,
    textAlign: 'center', 
    color:'#33929c',
  },
  add_icon:{
    position: 'relative', 
    left:'85%',
    bottom:'3%',
  },
  icon_bg:{
    backgroundColor: '#fff',
    width:25,
    height: 25,
    position: 'absolute',
    top:'25%',
    left:'3%',
  },
  t_row:{
    borderBottomColor: '#dedede',
    borderBottomWidth: 1,
    borderLeftColor: '#dedede',
    borderLeftWidth: 1,
    borderRightColor: '#dedede',
    borderRightWidth: 1,
    borderBottomColor: '#dedede',
    borderBottomWidth: 1,
  }
});
