const auditLog = {
  body:{
  	padding:20,
  },
  header:{
  	flexDirection: 'row',
  	borderWidth:0.5,
  	borderRadius:15,
  	borderColor: '#53b3bd'
  },
  headerLabel:{
  	width: '50%',
  	fontSize: 35,
  	fontWeight: 'bold',
  	padding:20
  },
  headerAmount:{
  	width: '50%',
  	backgroundColor: '#53b3bd',
  	borderTopRightRadius: 15,
  	borderBottomRightRadius: 15,
  	padding:20
  },
  headerPrice:{
  	fontSize: 35,
  	fontWeight: 'bold',
  	textAlign: 'right'
  },
  headerStartingBalance:{
  	textAlign: 'right',
  },
  btnTransfer:{
    width:'50%',
    textAlign: 'center',
    backgroundColor: '#53b3bd',
    color:'#fff',
    marginLeft:10,
  },
  btnEdit:{
    width:'50%',
    textAlign: 'center',
    backgroundColor: '#53b3bd',
    color:'#fff',
  },
  textLabel:{
    fontSize: 25,
    width:'48%',
  },
}


export {
  auditLog,
}