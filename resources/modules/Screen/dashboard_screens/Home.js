
import React from 'react';
import { ScrollView, Linking,
         Header, View,
         Text, Button,
         TextInput, StyleSheet,
         ToastAndroid, AsyncStorage,
         Picker, Image,
         TouchableNativeFeedback } from 'react-native';
import { Modal, ModalContent,
         BottomModal, ModalFooter,
         ModalButton }  from 'react-native-modals';
import { MaterialIcons, AntDesign,
         FontAwesome, MaterialCommunityIcons,
         FontAwesome5, Fontisto,
         Entypo } from 'react-native-vector-icons';

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";

// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import Toast         from 'react-native-simple-toast';

import moment        from 'moment'

import { formatDate, formatPrice,
         dateToYMD, formatGetDayMedium,
         monthYear, emptyObject,
         capitalize, filterYear,
         filterMonth } from '../../helpers/filters'
import { dashboard, transactionForm } from '../../helpers/styles'

import _ from 'lodash'

const initialState = {
    date_filter: '',
    view:'dashboard',
    transactions:{},
    dates:[],
    wallets:[],
    summary:{
      business:{
        income:0,
        expense:0,
      },
      personal:{
        income:0,
        expense:0,
      },
      all: {
        income:0,
        expense:0,
      }
    },
    pastSummary:{
      business:{
        income:0,
        expense:0,
      },
      personal:{
        income:0,
        expense:0,
      },
      all: {
        income:0,
        expense:0,
      }
    },
    total_amount_debit:0,
    total_amount_credit:0,
    old_total_amount_debit:0,
    old_total_amount_credit:0,
    modalVisible:false,
    filter:{
      year:2020,
      month:1,
      currency:'PHP',
    },
    currencies:[],
    pageLoader:false,
    cardLoader:false,
};

export default class Home extends React.Component {

  _isMounted = false;

  constructor(props){
    super(props);
    const presentDate = new Date();
    this.state         = initialState;
    
    if (global.user == null) {
      this.state = initialState;
      this.props.navigationProp('Login')
      return
    }

    initialState.date_filter  = dateToYMD(new Date())
    initialState.filter.year  = presentDate.getFullYear()
    initialState.filter.month = presentDate.getMonth() + 1
    initialState.currencies   = global.user.auth_organization.currencies

    if (initialState.currencies.length) {
      initialState.filter.currency = initialState.currencies[0].code
    }

    this.state         = initialState;
    global.view        = 'dashboard_home'
  }

  async reloadPage() {
    global.reload_page = this
    this.pageLoader(true)

    const currencies = global.user.auth_organization.currencies
    let state = this.state
    state.currencies = currencies
    
    if (state.currencies.length) {
      state.filter.currency = state.currencies[0].code
    }

    await this.setState(state)

    await this.getWallets();
    await this.getTransactions();
    this.pageLoader(false)
  }

  pageLoader(value) {
    let state = this.state
    state.pageLoader = value
    this.setState(state)
  }

  cardLoader(value) {
    let state = this.state
    state.cardLoader = value
    this.setState(state)
  }

  clearStates() {
    let state = this.state
    state.transactions = {}
    state.dates = []
    this.setState(state)
  }

  async getTransactions(fromFilter = 'modal') {
    const user  = global.user;
    const dateFilter = `${this.state.filter.year}-${this.state.filter.month}-01`

    let data = {
      currency        : this.state.filter.currency,
      date_filter     : dateFilter,
      organization_id : user.auth_organization.organization_id,
      role            : user.role.name
    }

    if ('proceed' === fromFilter) {
      data.date_filter = this.state.date_filter
      const year = filterYear(dateFilter)
      const month = filterMonth(dateFilter)
    }

    if (undefined == data.organization_id) {
      data.organization_id = user.auth_organization.id
    }

    this.cardLoader(true)

    this.clearStates()

    try {
      const response = await axios.post(global.app_url+'/api/a/transactions', data)
      let state = this.state
      if (response.data.mappedTransaction) {
        state.transactions = response.data.mappedTransaction
        _.forOwn(state.transactions,function(transaction, date){
          state.dates.push(date)  
        })
        state.summary = response.data.summary
        state.pastSummary = response.data.past_summary
        if ('modal' === fromFilter) {
          let month = this.state.filter.month
          if (this.state.filter.month < 10) {
            month = `0${parseInt(this.state.filter.month)}`
          }
          const date = `${this.state.filter.year}-${month}-01`
          state.date_filter = date
        }else if ('proceed' === fromFilter) {
          let filter   = state.filter
          filter.year  = parseInt(this.state.date_filter.split('-')[0])
          filter.month = parseInt(this.state.date_filter.split('-')[1])
          state.filter = filter
        }

        this.setState(state)
        this.cardLoader(false)

      } else {
        this.props.navigationProp('Login')
        this.cardLoader(false)
      }
    } catch(e) {
      console.log(e)
      this.cardLoader(false)

    }

  }

  async getWallets() {

    const user  = global.user;

    let data = {
      currency        : this.state.filter.currency,
      organization_id : user.auth_organization.organization_id,
      role            : user.role.name
    }

    if (undefined == data.organization_id) {
      data.organization_id = user.auth_organization.id
    }

    try {
      let all = []
      const response = await axios.post(global.app_url+'/api/a/wallets', data)
      let state = this.state
      const wallets = response.data.list
      _.forOwn(wallets,function(wallet) {
        _.forOwn(wallet,function(wal){
          if (wal.name.toLowerCase().includes('system-wallet-')) {
            return true
          }
          all.push(wal)
        })
      })
      state.wallets = all
      this.setState(state)
    } catch(e) {
      console.log(e)
    }
  }

  componentDidMount() {

    this._isMounted = true;

    if (this._isMounted) {
      this.props.onRef(this);
      if (global.user) {
        this.reloadPage()
      }
    }

  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  // filter date start
  monthProceed(action) {
    let date = null
    if (action == 'next') {
      date = moment(String(this.state.date_filter)).add(1, 'M').format('MMMM DD, Y')
    }else if (action == 'prev') {
      date = moment(String(this.state.date_filter)).add(-1, 'M').format('MMMM DD, Y')
    }
    let state = this.state
    state.date_filter = dateToYMD(new Date(date))
    this.setState(state)
    this.getTransactions('proceed')
  }
  // filter date end

  goToPage() {
    Linking.canOpenURL(global.app_url + registrationPage).then(supported => {
      if (supported) {
        Linking.openURL(global.app_url + registrationPage);
      } else {
        console.log("Don't know how to open URI: " + global.app_url + registrationPage);
      }
    });
  }

  changeView(view) {
    var _this = this
    if ('entry' !== view) {
      this.props.changeView(view);
    }
  }

  selectTransaction(transaction) {
    this.props.selectTransaction(transaction);
  }

  renderTransactionList(item) {
   const component = 
      <View style={{}}  key={item.transaction_id}>
        <TouchableNativeFeedback
          onPress={() => this.selectTransaction(item)}
          background={TouchableNativeFeedback.Ripple('#dedede', false)}>
          <View style={{flexDirection: 'row',width:'100%',marginTop:10, }}>
            <View  style={styles.mt_description}>
              <Text style={{fontSize:15,}}>{item.source.description}</Text>
              <View style={styles.mt_moreDetail}>
                <View style={{width:'100%',flexDirection: 'row'}}>
                  <Text style={{width:'50%',}}>
                    {item.source.category ? <Text style={styles.mt_category}><Entypo name="archive" size={12} /> {item.source.category.name}</Text> : <Text style={styles.mt_category} onPress={() => this.selectTransaction(item)}><MaterialIcons name="merge-type" size={12} />  {capitalize(item.type)}</Text>}
                  </Text>
                  <Text style={{width:'50%',}}>
                    {item.source.merchant ? <Text style={styles.mt_category}><Entypo name="shop" size={12} /> {item.source.merchant}</Text> : null}
                  </Text>
                </View>
              </View>  
            </View>
            <View style={styles.mt_price}>
              <Text style={this.listColor(item.type, 'amount')} >
                {formatPrice(item.amount.value)}
              </Text>
              <Text style={this.listColor(item.type, 'currency')} >{item.amount.currency}</Text>
            </View>
          </View>
        </TouchableNativeFeedback>
      </View>
    
    return (component)
  }

  listColor(type, style) {
    if ('income' === type || 'receive' === type) {
      if ('currency' === style) {
        return styles.mt_currency_label_income
      }else if ('amount' === style) {
        return styles.mt_income
      }
    }else if ('expense' === type || 'transfer' === type) {
      if ('currency' === style) {
        return styles.mt_currency_label_expense
      }else if ('amount' === style) {
        return styles.mt_expense
      }
    }
  }

  differenceBetween(first_value, second_value){
    var value = first_value - second_value
    if (value < 0) {
      return formatPrice(value * -1)
    }
    return formatPrice(value);
  }

  calculatePercentage(first_value, second_value){
    if (second_value == 0) {
      return 100
    }else{
      return ((first_value / second_value) * 100).toFixed(2)
    }
  }

  setModalVisible(value = null) {
    let state = this.state
    if (value) {
      state.modalVisible = value
    }else{
      state.modalVisible = !this.state.modalVisible
    }

    this.setState(state)
  }

  setSelecteFilter(itemValue, type) {
    let state = this.state
    state.filter[type] = itemValue
    this.setState(state)
  }

  applyFilter() {
    this.getTransactions()
    this.setModalVisible()
  }

  walletComponent(type) {
    let component = []
    const _this = this
    const wallets = this.state.wallets
    wallets.map(function(wallet, key){
      if (type === wallet.type) {
        component.push(
          <View key={key} style={{marginBottom: 5,paddingBottom: 5,paddingTop: 5, borderBottomColor: '#dedede',borderBottomWidth: 1, flexDirection: 'row',}}>
            <Text>{wallet.name}</Text>
            <Text style={{position: 'absolute',right:'0%' }}>{_this.state.filter.currency} {formatPrice(wallet.balance)}</Text>
          </View>
        )
      }

    })  
    return (component)
  }

  modalFilter() {

    const presentYear = parseInt(new Date().getFullYear());
    let yearOption = [];
    for (let i = 2020; i <= presentYear; i++) {
        yearOption.push(<Picker.Item value={i} key={i} label={`${i}`}/>)
    }

    const modalComponent = 
      <BottomModal
        visible={this.state.modalVisible}
        onTouchOutside={() => {
          this.setModalVisible(false);
        }}
        onSwipeOut={() => this.setModalVisible()}
      >
      <View style={{backgroundColor: '#f3f3f3'}}>
        <View style={formStyle.cardBody}>
          <View style={formStyle.rowMore}>
            <Text style={{width:'100%'}} >Filters</Text>
          </View>
          <View style={formStyle.rowCard}>
            <Fontisto style={formStyle.rowIcon} name="date" size={20} />
            <Text style={formStyle.rowLabel}>Year</Text>
            <View style={formStyle.rowValue}>
              <Picker
                style={formStyle.pickerInputCard}
                selectedValue={this.state.filter.year}
                onValueChange={(itemValue, itemIndex) => this.setSelecteFilter(itemValue, 'year')}
              >
                {yearOption}
              </Picker>
            </View>
          </View>
          <View style={formStyle.rowCard}>
            <Fontisto style={formStyle.rowIcon} name="date" size={20} />
            <Text style={formStyle.rowLabel}>Month</Text>
            <View style={formStyle.rowValue}>
              <Picker
                style={formStyle.pickerInputCard}
                selectedValue={this.state.filter.month}
                onValueChange={(itemValue, itemIndex) => this.setSelecteFilter(itemValue, 'month')}
              >
              {moment.months().map((month, key) => {
                return <Picker.Item value={key+1} key={key} label={month}/>
              })}
              </Picker>
            </View>
          </View>
          <View style={formStyle.rowCard}>
            <Entypo style={formStyle.rowIcon} name="wallet" size={20} />
            <Text style={formStyle.rowLabel}>Currency</Text>
            <View style={formStyle.rowValue}>
              <Picker
                style={formStyle.pickerInputCard}
                selectedValue={this.state.filter.currency}
                onValueChange={(itemValue, itemIndex) => this.setSelecteFilter(itemValue, 'currency')}
              >
              {this.state.currencies.map((currency, key) => {
                return <Picker.Item value={currency.code} key={key} label={currency.code}/>
              })}
              </Picker>
            </View>
          </View>
          <View style={{width:'100%',marginTop: 20}}>
            <Button title="Apply Filter" style={{width:'100%'}} onPress={() => this.applyFilter()} color={'#53b3bd'} />
          </View>
        </View>
      </View>
      </BottomModal>

    return (modalComponent)
  }

  iconSummary(type) {
    let component = null
    if ('personal-income' === type) {
      if (this.state.summary.personal.income < this.state.pastSummary.personal.income) {
        component = <AntDesign name="arrowup" color='green' size={10} />
      }else{
        component = <AntDesign name="arrowdown" color='red' size={10} />
      }
    }else if('personal-expense' === type){
      if (this.state.summary.personal.expense < this.state.pastSummary.personal.expense) {
        component = <AntDesign name="arrowdown" color='green' size={10} />
      }else{
        component = <AntDesign name="arrowup" color='red' size={10} />
      }
    }else if ('business-income' === type) {
      if (this.state.summary.business.income > this.state.pastSummary.business.income) {
        component = <AntDesign name="arrowup" color='green' size={10} />
      }else{
        component = <AntDesign name="arrowdown" color='red' size={10} />
      }
    }else if ('business-expense' === type) {
      if (this.state.summary.business.expense > this.state.pastSummary.business.expense) {
        component = <AntDesign name="arrowup" color='red' size={10} />
      }else{
        component = <AntDesign name="arrowdown" color='green' size={10} />
      }
    }else if ('all-income' === type) {
      if (this.state.summary.all.income > this.state.pastSummary.all.income) {
        component = <AntDesign name="arrowup" color='green' size={10} />
      }else{
        component = <AntDesign name="arrowdown" color='red' size={10} />
      }
    }else if ('all-expense' === type) {
      if (this.state.summary.all.expense > this.state.pastSummary.all.expense) {
        component = <AntDesign name="arrowup" color='red' size={10} />
      }else{
        component = <AntDesign name="arrowdown" color='green' size={10} />
      }
    }
    return (component)
  }

  summaryStyle(type) {
    if ('personal-income' === type) {
      if (this.state.summary.personal.income < this.state.pastSummary.personal.income) {
        return styles.compared_text_up
      }else{
        return styles.compared_text_down
      }
    }else if('personal-expense' === type){
      if (this.state.summary.personal.expense < this.state.pastSummary.personal.expense) {
        return styles.compared_text_up
      }else{
        return styles.compared_text_down
      }
    }else if ('business-income' === type) {
      if (this.state.summary.business.income > this.state.pastSummary.business.income) {
        return styles.compared_text_up
      }else{
        return styles.compared_text_down
      }
    }else if ('business-expense' === type) {
      if (this.state.summary.business.expense > this.state.pastSummary.business.expense) {
        return styles.compared_text_down
      }else{
        return styles.compared_text_up
      }
    }else if ('all-income' === type) {
      if (this.state.summary.all.income > this.state.pastSummary.all.income) {
        return styles.compared_text_up
      }else{
        return styles.compared_text_down
      }
    }else if ('all-expense' === type) {
      if (this.state.summary.all.expense > this.state.pastSummary.all.expense) {
        return styles.compared_text_down
      }else{
        return styles.compared_text_up
      }
    }
  }

  transactionBody() {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{padding:10,marginBottom: '35%'}}>
          <View style={styles.transactionCard}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flexDirection: 'row',width:'80%',marginLeft:5, marginTop:5,marginBottom:5,}}>
                  <FontAwesome5 onPress={() => {
                  this.monthProceed('prev')
                }} name="angle-double-left" color='black' size={20} />
                  <Text style={{marginLeft:20,marginRight:20,}}>
                    {monthYear(this.state.date_filter)}
                  </Text>
                  <FontAwesome5 onPress={() => {
                  this.monthProceed('next')
                }} name="angle-double-right" color='black' size={20} />
              </View>
              <FontAwesome5 onPress={() => this.setModalVisible()} name="ellipsis-h" size={20} style={styles.filtersBtn} />
            </View>
            {this.state.cardLoader ? (
                <Image style={{ width: 100, height: 50,marginLeft:'38%', resizeMode: 'contain' }} source={require('../../../../assets/Spinner-0.3s-58px.gif')} />
              ) : this.transactionContainer()}
          </View>
        </View>
      </ScrollView>
    )
  }

  transactionContainer() {

    const _this = this
    const transactions = this.state.transactions
    let showTransactionNull = null
    if (transactions) {
      showTransactionNull = !emptyObject(transactions)
    }

    return (
      <View style={styles.transactions_container}>
      {showTransactionNull ? this.state.dates.map(function(date, key) {
        return <View key={key}>
            <View style={styles.mt_row} >
              <Text><Text style={styles.sd_day}>{formatDate(date)}</Text>  {(formatGetDayMedium(date))}</Text>
            </View>
            {transactions[date].map((item)=> (_this.renderTransactionList(item)) )}
          </View>
      }) : (<Text>No transactions for this month.</Text>)}
      </View>
    )
  }

  render() {
    return (
      <View style={styles.body}>
        <View style={styles.dashboardSummary}>
          <ScrollView
            style={{}}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            <View style={styles.c_card_income}>
              <Text style={styles.c_label_type}>All Income</Text>
              <Text style={styles.c_currency}>PHP</Text>
              <Text style={styles.c_price}>{formatPrice(this.state.summary.all.income)}</Text>
            </View>
            <View style={styles.c_card_expense}>
              <Text style={styles.c_label_type}>All Expense</Text>
              <Text style={styles.c_currency}>PHP</Text>
              <Text style={styles.c_price}>{formatPrice(this.state.summary.all.expense)}</Text>
            </View>
            <View style={styles.c_card_income}>
              <Text style={styles.c_label_type}>Business Income</Text>
              <Text style={styles.c_currency}>PHP</Text>
              <Text style={styles.c_price}>{formatPrice(this.state.summary.business.income)}</Text>
            </View>
            <View style={styles.c_card_expense}>
              <Text style={styles.c_label_type}>Business Expense</Text>
              <Text style={styles.c_currency}>PHP</Text>
              <Text style={styles.c_price}>{formatPrice(this.state.summary.business.expense)}</Text>
            </View>
            <View style={styles.c_card_income}>
              <Text style={styles.c_label_type}>Personal Income</Text>
              <Text style={styles.c_currency}>PHP</Text>
              <Text style={styles.c_price}>{formatPrice(this.state.summary.personal.income)}</Text>
            </View>
            <View style={styles.c_card_expense}>
              <Text style={styles.c_label_type}>Personal Expense</Text>
              <Text style={styles.c_currency}>PHP</Text>
              <Text style={styles.c_price}>{formatPrice(this.state.summary.personal.expense)}</Text>
            </View>
          </ScrollView>
        </View>
        {this.state.pageLoader ? (
          <Image style={{ width: 100, height: 50,marginLeft:'38%',marginTop:'10%', resizeMode: 'contain' }} source={require('../../../../assets/Spinner-0.3s-58px.gif')} />
        ) : this.transactionBody()}
        {this.modalFilter()}
      </View>
    );
  }

}

const styles = StyleSheet.create(dashboard);
const formStyle = StyleSheet.create(transactionForm);
