
import React from 'react';
import { Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid, TouchableNativeFeedback } from 'react-native';
import Modal from 'react-native-modal';

import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';

import { NavigationContainer }              from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons               from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons                        from 'react-native-vector-icons/MaterialIcons';
import DropDownPicker                       from 'react-native-dropdown-picker';
import OrganizationModalSelect              from './component/OrganizationModalSelect';

import axios  from 'axios';

const initialState = {
  org_selected:'org1',  
  organizations:[],
}

export default class OrganizationPicker extends React.Component {

  constructor(props){
    super(props);
    if (global.user == null) {
      this.state = initialState;
      return
    }
    if (global.user.auth_organization) {
      initialState.org_selected  = global.user.auth_organization.name
    }
    initialState.organizations = global.user.organizations
    this.state = initialState;
    global.org_select          = this 
  }

  async selectItem(item){
    let _this              = this
    let state              = _this.state
    let user               = global.user
    user.auth_organization = item.data
    state.org_selected     = item.label
    global.user            = user
    
    await _this.setState(state)
   
    await global.reload_page.reloadPage()
    global.navigation.navigationProp('Core')
  }

  changeOrganization()
  {
    this.organizationModal.openModal()
  }

  render() {
    return (
      <TouchableNativeFeedback
        onPress={() => this.changeOrganization()}
        background={TouchableNativeFeedback.Ripple('#dedede', false)}>
        <View style={{flexDirection: 'row'  }}>
          <Text style={{marginRight:5,fontSize: 15,marginTop:2}} >{this.state.org_selected}</Text>
          <MaterialIcons  style={{marginRight:5,marginTop:2}} name="keyboard-arrow-down" color='black' size={25} />
          <OrganizationModalSelect selectItem={this.selectItem.bind(this)}  onRef={ref => (this.organizationModal = ref)} />
        </View>
      </TouchableNativeFeedback>
    );
  }
}