
import React from 'react';
import { Platform, ScrollView, Linking, Header, View, Text, Button, TextInput, StyleSheet, ToastAndroid, Picker, KeyboardAvoidingView } from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import { Colors}                from 'react-native/Libraries/NewAppScreen';
import { FloatingAction }       from "react-native-floating-action";
import DateTimePicker           from '@react-native-community/datetimepicker';


// import styles from '../../../resources/styles/main.js';
import axios         from 'axios';
import Config        from "react-native-config";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import AntDesign     from 'react-native-vector-icons/AntDesign';
import moment        from 'moment'
import Toast         from 'react-native-simple-toast';

const initialState = {
  'view':'edit',
  active_wallet:{},
  wallets:[],
  wallet_id:'',
  transfer:{
    from_wallet_currency:'',
    from_wallet_current_amount:'',
    from_wallet_id:'',
    to_wallet_id:'',
    transfer_amount:'',
    transaction_date:'',
    description:'',
  },
  showDate:false,
};

export default class Transfer extends React.Component {

  constructor(props){
    super(props);
    var wallet                                       = this.activeWalletModel()
    initialState.active_wallet                       = wallet
    initialState.wallet_id                           = this.activeWallet()
    var wallets                                      = this.activeWallets();
    var parsed_wallets                               = [];
    wallets.map((t_wallet,key) =>{
      if (t_wallet.id != wallet.id) {
        parsed_wallets.push(t_wallet)
      }
    })
    initialState.wallets                             = parsed_wallets
    initialState.transfer.transaction_date           = this.dateToYMD(new Date())
    initialState.transfer.from_wallet_id             = 'parent_'+wallet.id
    initialState.transfer.from_wallet_currency       = wallet.currency
    initialState.transfer.from_wallet_current_amount = wallet.current_amount
    if (initialState.wallets[0]) {
      initialState.transfer.to_wallet_id               = 'parent_'+initialState.wallets[0].id
    }
    this.state                 = initialState;
  }

  fromParent(){
    this.transfer()
  }

  dateToYMD (date) {
      var d = date.getDate();
      var m = date.getMonth() + 1; //Month from 0 to 11
      var y = date.getFullYear();
      return '' + y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
  }

  activeWallet()
  {
    return this.props.activeWallet()
  }


  activeWallets()
  {
    return this.props.activeWallets()
  }

  activeWalletModel()
  {
    return this.props.activeWalletModel()
  }

  setSelecteOption(selected, field){
    var _this = this
    var state = _this.state
    state.transfer[field] = selected
    _this.setState(state)
  }

  formatPrice(value) {
    if (value) {
        let val = (value/1).toFixed(2).replace(',', '.')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }else{
        return '0.00'
    }
  }

  changeView(view){
    this.props.changeView(view);
  }

  setWalletModel(wallet){
    this.props.setWalletModel(wallet);
  }

  transfer(){
    var _this = this
    var data = _this.state.transfer
    var validate_field = true
    if (
      !data.from_wallet_id ||
      !data.to_wallet_id ||
      !data.transfer_amount ||
      !data.transaction_date ||
      !data.description 
    ) {
      validate_field = false
    }
    if (!validate_field) {
      Toast.show('Fill up required fields');
      return;
    }
    if (validate_field) {
      axios.post(global.app_url+'/api/a/wallet/transfer',data).then(function(response){
        _this.setWalletModel(response.data)
        _this.changeView('activities_'+_this.state.wallet_id)
        Toast.show('Transfer success!');

      }).catch(function(error){
        console.log(error)
        Toast.show(error.response.data);
      })
    }
  }


  setSelectedDate(date){
    var _this = this
    if (date) {
      _this.setSelecteOption(_this.dateToYMD(new Date(date)),'transaction_date')
    }
    var state      = _this.state
    state.showDate = false
    _this.setState(state)
  }

  showDate()
  {
    var _this      = this
    var state      = _this.state
    state.showDate = true
    _this.setState(state)
  }

  render() {
    // /api/a/wallet/transfer
    var from_wallet = null
    var disable_from_id = this.state.active_wallet.wallet_pockets.length ? true : false 
    if (this.state.active_wallet.wallet_pockets.length) {
      from_wallet = this.state.active_wallet.wallet_pockets.map((pocket,key) => {
          return <Picker.Item key={key} value={'children_'+pocket.id} label={' - '+pocket.name+' ('+this.state.active_wallet.currency+' '+pocket.amount+')'} />
      });
    }
    var active_wallet = this.state.active_wallet
    var to_wallet = this.state.wallets.map((to_wallet,to_key) => {
          return <Picker.Item key={to_key} value={'parent_'+to_wallet.id} label={to_wallet.name+' ('+to_wallet.currency+' '+to_wallet.current_amount+')'} />
      });
    return (
      <KeyboardAvoidingView 
      behavior={"padding"}
      style={{flex:1}}
      >
      <View style={styles.body}>
        <ScrollView style={styles.form_input}>
          <View style={styles.form_container}>
            <Text style={styles.fi_label}>From Wallet <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_row}>
            <View
              style={styles.fi_half_picker}
              >
                <Picker
                  selectedValue={this.state.transfer.from_wallet_id}
                  onValueChange={(itemValue, itemIndex) => this.setSelecteOption(itemValue,'from_wallet_id')}
                >
                <Picker.Item value={'parent_'+this.state.active_wallet.id} disabled={disable_from_id} label={this.state.active_wallet.name+' ('+this.state.active_wallet.currency+' '+this.state.active_wallet.current_amount+')'} />
                {from_wallet}
                </Picker>
              </View>
            </View>
            <Text style={styles.fi_label}>Current Amount ({this.state.transfer.from_wallet_currency}) <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_row}>
                <TextInput  
                  defaultValue={this.formatPrice(this.state.transfer.from_wallet_current_amount)}
                  editable={false}
                  style={styles.fi_input_full_width}  
                />  
            </View>
            <Text style={styles.fi_label}>To Wallet <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_row}>
              <View
                style={styles.fi_half_picker}
                >
                  <Picker
                    selectedValue={this.state.transfer.to_wallet_id}
                    onValueChange={(itemValue, itemIndex) => this.setSelecteOption(itemValue,'to_wallet_id')}
                  >
                  {to_wallet}
                  </Picker>
              </View>
            </View>
            <Text style={styles.fi_label}>Transfer Amount (PHP) <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_row}>
                <TextInput  
                  style={styles.fi_input_full_width}  
                  selectedValue={this.state.transfer.transfer_amount}
                  onChangeText={(inputText) => this.setSelecteOption(inputText,'transfer_amount')}
                  keyboardType={'numeric'}  
                />  
            </View>
              <Text style={styles.fi_r_date_label}>Date <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_row}>
              <View style={styles.fi_r_date_input}>
                <Text  onPress={() => this.showDate()}>{this.state.transfer.transaction_date} </Text>
              </View>
              {this.state.showDate ? (<DateTimePicker
                testID="dateTimePicker"
                mode={'calendar'}
                value={new Date(this.state.transfer.transaction_date)}
                display="default"
                onChange={(event, date) => {
                  this.setSelectedDate(date)
                }}
              />) : null }
            </View>
            <Text style={styles.fi_label}>Description <Text style={{color:'red'}}>*</Text></Text>
            <View style={styles.fi_row}>
                <TextInput  
                  selectedValue={this.state.transfer.description}
                  onChangeText={(inputText) => this.setSelecteOption(inputText,'description')}
                  style={styles.fi_input_full_width}
                />  
            </View>
          </View>
        </ScrollView>
      </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  body: {
    padding:10,
  },
  form_input:{
    height: '87%',
    borderWidth: 1,
    borderColor:'#c4eaef',
    padding:10,
  },
  form_container:{
    marginBottom:25,
  },
  fi_row:{
    flexDirection: 'row', 
    marginTop: 10,
    marginBottom: 10,
  },
  fi_picker:{
    height: 50,
    marginRight: 20,
    width: '30%',
    borderWidth: 1,
    borderColor: '#dedede',
  },
  fi_picker_full_width:{
    width: '100%',
    borderWidth: 1,
    borderColor: '#dedede',
  },
  fi_textinput:{
    borderWidth: 1,
    borderColor: '#dedede',
    width: '65%',
    height: 50,
    padding:5,
    marginTop:0,
  },
  fi_label:{
  },
  fi_input_full_width:{
    borderWidth: 1,
    borderColor: '#dedede',
    width: '100%',
    height: 50,
    padding:5,
  },
  fi_input_half_width:{
    borderWidth: 1,
    borderColor: '#dedede',
    width: '75%',
    height: 35,
    padding:5,
  },
  fi_r_date_label:{
    width: '25%',
    marginTop: 6,
  },
  fi_date_picker:{
    width: '75%',
  },
  fi_half_picker:{
    height: 50,
    width: '100%',
    borderWidth: 1,
    borderColor: '#dedede',
  },
  fi_r_label:{
    width: '100%',
    marginTop: 13,
  },
  fi_r_date_input:{
    borderWidth:0.5,
    borderColor:'black',
    width:'100%',
    padding:10,
  },
});
