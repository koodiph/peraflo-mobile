import React from 'react';
import { StyleSheet, Text, View, TextInput, Button, Image } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer }   from 'react-navigation';
import Login                    from './resources/modules/Auth/Login';
import Core                     from './resources/modules/Core/Core';
import OrganizationPicker       from './resources/modules/Core/OrganizationPicker';
import OrganizationSelection    from './resources/modules/Core/OrganizationSelection';


import Icon                     from 'react-native-vector-icons/FontAwesome5';
import MaterialIcons            from 'react-native-vector-icons/MaterialIcons';
import DropDownPicker           from 'react-native-dropdown-picker';
import { ModalPortal }          from 'react-native-modals';

function headerRight(){
  return (
    <OrganizationPicker />
    );
}

function headerLeft(){
  return (
    <Text style={{marginRight:10}}>Peraflo</Text>
    );
}


const AppNavigator = createStackNavigator({
  'Login': {
    screen: Login,
    name:'Login',
    navigationOptions: {
      title:null,
      headerLeft: () => <Image style={{ width: 100, height: 50, marginLeft:15, resizeMode: 'contain' }} source={require('./assets/logo.png')} />,
      headerForceInset: { top: 'never', bottom: 'never' },
    },
  },
  'Core': {
    screen: Core,
    name:'Core',
    navigationOptions: {
      title:null,
      headerLeft: () => <Image style={{ width: 100, height: 50,marginLeft:15, resizeMode: 'contain' }} source={require('./assets/logo.png')} />,
      headerRight: headerRight,
      headerForceInset: { top: 'never', bottom: 'never' },
    },
  },
  'Organizations': {
    screen: OrganizationSelection,
    name:'Organization',
    navigationOptions: {
      title:'Select Organization',
      headerRight: headerRight,
      headerForceInset: { top: 'never', bottom: 'never' },
    },
  },
},
  {
    initialRouteName: 'Login',
  }
);

export default createAppContainer(AppNavigator);

